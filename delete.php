<?php

session_start();

if (!isset($_SESSION['logged_in']))
   {
   header("location: logout.php");    
   }
else
   {

      include('connect.php');

      $pos = $_SESSION['position'];
      $sn = $_GET['sn'];
      $id = $_GET['id'];
      $lID = $_GET['lID'];
      //$AMOUNT = $_GET['amount'];
      // $emp = $_GET['employee'];
         
      if(! $conn ) {
         die('Could not connect: ' . mysqli_error());
      }
      echo $sn;   
      
      //DELETE SINGLE RECEIPT
      if($_GET['sn'] != 0)
      $sql = "DELETE FROM receipt WHERE sn = ". $sn;

      // else if($_GET['id'] != 0)
      // $sql = "DELETE FROM accounts WHERE idnumber = ". $id;

      if (mysqli_query($conn, $sql)) 
      {
         echo "<div class='uk-alert-danger' uk-alert><a class='uk-alert-close' uk-close></a>Record deleted successfully</div>";
      } 
      else 
      {
         echo "<div class = 'uk-alert-danger' uk-alert><a class='uk-alert-close' uk-close></a>Error deleting record: " . mysqli_error($conn);
      }

      if($_GET['lID'] != 0)
      {
         $sqlDelete = "DELETE FROM leave_tb WHERE leave_ID = ". $lID;
         mysqli_query($conn, $sqlDelete);

         header("location: leaveList.php");
      }
      

      if(isset($_POST['deleteall']))
      {
         $chkarr = $_POST['checkbox'];

         foreach($chkarr as $sn)
         {
            mysqli_query($conn, "DELETE FROM receipt WHERE sn = " .$sn);
         }
         header("Location:sample.php");

         foreach($chkarr as $id)
         {
            mysqli_query($conn, "DELETE FROM accounts WHERE idnumber = " .$id);
         }
      }
      mysqli_close($conn);
      
      if($_SERVER['HTTP_REFERER'] = "http://localhost/OJT/admin.php")
      {
         if($pos == "Admin")
         {
            header("Location: admin.php");
         }
         else
         {
            header("Location: sample.php");
         }  
      }
      
      if($_SERVER['HTTP_REFERER'] = "http://localhost/OJT/userlistpage.php")
      {
         header("location: userlistpage.php");
      }  
   }
?>