<?php
    include ('connect.php');
	
	if ($conn->connect_error) {
    	die("Connection failed: " . $conn->connect_error);
    } 

    else 
    {
        $sql = "SELECT * FROM leave_tb ORDER BY startDate ASC";
    }

	if ($result = mysqli_query($conn,$searchSQL))
	{
		echo "  <table style='width:100%;'>
                    <form method='POST' action='delete.php'>
                        <tr>
                            <th style='color:DodgerBlue; width:0.5% ;border-bottom: 2px solid #0dd; padding: 10px'>
                                <input type='submit' name='deleteall' value='Delete'; class='uk-button uk-button-primary uk-button-small'>
                            </th>
                            <th style='color:DodgerBlue; width:0.5% ;border-bottom: 2px solid #0dd;padding:0px'>Delete</th>
                            <th style='color:DodgerBlue; width:0.3% ;border-bottom: 2px solid #0dd;padding:0px'>Approval</th>
                            <th style='color:DodgerBlue; width:2% ;border-bottom: 2px solid #0dd;padding:5px'>Employee</th>
                            <th style='color:DodgerBlue; width:2% ;border-bottom: 2px solid #0dd;padding:5px'>Start</th>
                            <th style='color:DodgerBlue; width:2% ;border-bottom: 2px solid #0dd;padding:5px'>Return</th>
                            <th style='color:DodgerBlue; width:0.4% ;border-bottom: 2px solid #0dd;padding:5px'>Type</th>
                            <th style='color:DodgerBlue; width:10% ;border-bottom: 2px solid #0dd;padding:5px'>Reason</th>
                            <th style='color:DodgerBlue; width:2% ;border-bottom: 2px solid #0dd;padding:5px'>Condition</th>
                        </tr>";

		while($row = mysqli_fetch_array($result)) 
		{

                $leaveID = $row['leave_ID'];
				$user = $row['user'];
                $sDate =$row['startDate'];
                $sec = strtotime($sDate);
                $hsDate = date("Y-m-d H:i", $sec);
                $hsDate = $sDate . ":00"; 
                $eDate = $row['end_Date'];
                $sDate =new dateTime($sDate);
                $eDate =new dateTime($eDate);
				$leaveType = $row['leaveType'];
				$reason = $row['reason'];
                $approval = $row['approval'];



                if($approval == 1)
                {
                    $approvalText = "Approved";
                    $rowcolor= "#b3ffb3";
                }
                if ($approval == 0)
                {
                    $approvalText = "Pending"; 
                    $rowcolor = "#d9d9d9";
                }
                if ($approval == 2)
                {
                    $approvalText = "Declined";  
                    $rowcolor = "#ffb3b3";
                }
                if ($approval == 3)
                {
                    $approvalText = "On Going";  
                    $rowcolor = "#ffffb3";
                }

                if ($approval == 4)
                {
                    $approvalText = "Completed";  
                    $rowcolor = "#b3b3ff";
                }


				echo "	<tr>
							<td style='text-align:center;border-bottom: 1px solid #add;padding:8px'>
								<input type='checkbox' name='checkbox[]' value='".$leaveID."'>
							</td>
							<td style='text-align:center;border-bottom: 1px solid #add;padding:8px'>
							<a href='delete.php?lID=".$leaveID."'; onClick=\"return confirm('Are you sure you want to delete?');\" uk-icon='trash'; uk-toggle></a>
							</td> 
                            <form action='leaveList.php' method ='post' style = 'margin-bottom: 0px;' autocomplete='off'>
                                <td style='text-align:center;border-bottom: 1px solid #add;padding:15px'>
                                    <div style='display:inline'>
                                    <a href='approveLeave.php?id=".$leaveID."'; onClick=\"return confirm('Are you sure you wish to APPROVE of this request?');\" >
                                        <span style='background-color:#add' uk-icon='check'; input type= 'submit'></span>
                                    </a>
                                    <a href='declineLeave.php?id=".$leaveID."'; onClick=\"return confirm('Are you sure you wish to DECLINE of this request?');\">    
                                        <span style='background-color:#add' uk-icon='close'; input type= 'submit'></span>
                                    </a>
                                    </div>
                                </td>
                            </form>

							<td style='text-align:left;text-align:center;border-bottom: 1px solid #add;padding:8px'>".$user."</td>
							<td style='text-align:left;text-align:center;border-bottom: 1px solid #add;padding:8px'>".$sDate->format('M-d-Y')."</td>
							<td style='text-align:left;text-align:center;border-bottom: 1px solid #add;padding:8px'>".$eDate->format('M-d-Y')."</td>
							<td style='text-align:left;text-align:center;border-bottom: 1px solid #add;padding:8px'>".$leaveType."</td>
							<td style='text-align:left;text-align:center;border-bottom: 1px solid #add;padding:8px'>".$reason."</td>
							<td style='text-align:left;text-align:center;border-bottom: 1px solid #add;padding:8px;background-color:$rowcolor'>".$approvalText."</td>
						</tr>";
            
        }
		echo "</table>";
  		mysqli_free_result($result);
    }
	mysqli_close($conn);
?>