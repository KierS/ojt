<?php
session_start();
if($_SESSION['position'] == "Admin")
{
    
    error_reporting(E_PARSE | E_ERROR);

    include("connect.php");


    $sql = "SELECT * FROM accounts WHERE id = ". $_GET['id'];
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_array($result);
}

if($_SESSION['position'] == "Employee")
{
    header("location:sample.php");
}


?>

<html>
<head>
    
    <title> TAS Tradesoft - Expense Report </title>
    <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="stylesheet" href="css/uikit.css" />
            <script src="js/uikit.min.js"></script>
            <script src="js/uikit-icons.min.js"></script>
</head>
<body style="background-color:#c2c2d6">
    
<!-- Navigation Bar... -->
    <nav class="uk-navbar-container uk-margin" uk-navbar style="margin-bottom:0px;">
        <div class="uk-navbar-left">
            <div>
                <ul class="uk-navbar-nav">
                    <li>
                        <a href="sample.php">
                            <img src="TAS1.png" style= "width:120px; height:39px" alt=""> 
                        </a>
                    </li>
                    <li><a href="userlistpage.php"  uk-icon="user">User List</a></li>  
                </ul>
            </div>
        </div>

        <div class="uk-navbar-right">

            <ul class="uk-navbar-nav" action ="logout">
                
                <li>
                    <a href="user.php" uk-icon="users">
                    </a>
                </li>
                <li>
                    <a href="register.php" uk-icon="plus-circle">
                    </a>
                </li>
                <li>
                    <a href="logout.php">Log-Out</a>
                </li>
            </ul>
        </div>
    </nav>
<!-- Navigation Bar End -->
    <div style ="margin: auto; width: 50%; height:100%; background-color:white; padding:20px;">
        <p style="font-size:40px; font-family: Helvetica; color: #4da6ff; padding:0; margin:0"> <?php echo $row['first_name']." " .$row['last_name']; ?> </p>
        <p style="font-size:20px; font-color:4da6f0; font-family: Helvetica; padding:0;margin:0"> <?php echo $row['department']; ?> </p>
        <hr>
        <?php 
                $birthdate =new dateTime($row['birthdate']);
                $datehired = new dateTime($row['datehired']); 
                if($_GET['edit'] == 'true')
                {
                    echo "
                    <form class='uk-form-horizontal uk-margin-small' action='userlistpage.php' method='POST' autocomplete='off'>    
                            <table style='width:100%' >
                                <tr>
                                    <td style='font-size:18px; width:35%'>ID Number:</td>
                                    <td style='font-size:18px'><input class='uk-input uk-form-width-large' type='text' name='idnumber' placeholder='".$row['idnumber']."'></td>
                                </tr>
                                <tr>
                                    <td style='font-size:18px'>First Name:</td>
                                    <td style='font-size:18px'><input class='uk-input uk-form-width-large' type='text' name='first_name' placeholder='".$row['first_name']."'></td>
                                </tr>
                                <tr>
                                    <td style='font-size:18px'>Last Name:</td>
                                    <td style='font-size:18px'><input class='uk-input uk-form-width-large' type='text' name='last_name' placeholder='".$row['last_name']."'></td>
                                </tr>
                                <tr>
                                    <td style='font-size:18px'>Birthdate:</td>
                                    <td style='font-size:18px'><input class='uk-input uk-form-width-large' type='date' name='birthdate' placeholder='".$birthdate->format('M-d-Y')."'></td>
                                </tr>
                                <tr>
                                    <td style='font-size:18px'>Home Address:</td>
                                    <td style='font-size:18px'><input class='uk-input uk-form-width-large' type='text' name='home_Address' placeholder='".$row['home_Address']."'></td>
                                </tr>
                                <tr>
                                    <td style='font-size:18px'>Sick Leaves Remaining:</td>
                                    <td style='font-size:18px'><input class='uk-input uk-form-width-large' type='text' name='sick_Leave' placeholder='".$row['sick_Leave']."'></td>
                                </tr>
                                <tr>
                                    <td style='font-size:18px'>Vacation Leaves Remaining:</td>
                                    <td style='font-size:18px'><input class='uk-input uk-form-width-large' type='text' name='vac_Leave' placeholder='".$row['vac_Leave']."'></td>
                                </tr>
                                <tr>
                                    <td style='font-size:18px'>Position:</td>
                                    <td>
                                        <select class='uk-select uk-form-width-large' name='position' placeholder='".$row['position_type']."'>
                                            <option value = 'Admin'>Admin</option>
                                            <option value = 'Employee'>Employee</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='font-size:18px'>PHIC: </td>
                                    <td style='font-size:18px'><input class='uk-input uk-form-width-large' type='text' name='phic' placeholder='".$row['phic']."'></td>
                                </tr>
                                <tr>
                                    <td style='font-size:18px'>Pag-Ibig: </td>
                                    <td style='font-size:18px'><input class='uk-input uk-form-width-large' type='text' name='pagibig' placeholder='".$row['pagibig']."'></td>
                                </tr>
                                <tr>
                                    <td style='font-size:18px'>SSS: </td>
                                    <td style='font-size:18px'><input class='uk-input uk-form-width-large' type='text' name='sss' placeholder='".$row['sss']."'></td>
                                </tr>
                                <tr>
                                    <td style='font-size:18px'>Tin: </td>
                                    <td style='font-size:18px'><input class='uk-input uk-form-width-large' type='text' name='tin' placeholder='".$row['tin']."'></td>
                                </tr>
                                <tr>
                                    <td style='font-size:18px'>Department: </td>
                                    <td style='font-size:18px'><input class='uk-input uk-form-width-large' type='text' name='department' placeholder='".$row['department']."'></td>
                                </tr>
                                <tr>
                                
                                    <td style='font-size:18px'>Date of Employment: </td>
                                    <td style='font-size:18px'><input class='uk-input uk-form-width-large' type='date' name='datehired' placeholder='".$datehired->format('M-d-Y')."'></td>
                                </tr>
                                <tr>
                                    <td style='font-size:18px'>Quota: </td>
                                    <td style='font-size:18px'><input class='uk-input uk-form-width-large' type='text' name='requiredamount' placeholder='".$row['requiredamount']."'></td>
                                </tr>
                                <input type = 'hidden'; value ='".$row['id']."' ; name='idofficial' />

                            </table>
                            <hr>
                            <input type='submit'; value='Submit'; name='editThis'; class='uk-button uk-button-primary'>
                        </form>";
                }
                else
                {
                    $birthdate =new dateTime($row['birthdate']);
                    $datehired = new dateTime($row['datehired']); 
                    echo "
                        <table style='width:100%'>
                            <tr>
                            <td style='font-size:18px; padding-top:8px; width:35%'>ID Number:</td>
                                <td style='font-size:18px'>".$row['idnumber']."</td>
                            </tr>
                            <tr>
                                <td style='font-size:18px; padding-top:16px'>First Name:</td>
                                <td style='font-size:18px'>".$row['first_name']."</td>
                            </tr>
                            <tr>
                            <td style='font-size:18px; padding-top:16px'>Last Name:</td>
                                <td style='font-size:18px'>".$row['last_name']."</td>
                            </tr>
                            <tr>
                            <td style='font-size:18px; padding-top:16px'>Birthdate:</td>
                                <td style='font-size:18px'>".$birthdate->format('M-d-Y')."</td>
                            </tr>
                            <tr>
                            <td style='font-size:18px; padding-top:16px'>Home Address:</td>
                                <td style='font-size:18px'>".$row['home_Address']."</td>
                            </tr>
                            <tr>
                                <td style='font-size:18px; padding-top:16px'>Sick Leaves Remaining:</td>
                                <td style='font-size:18px'>".$row['sick_Leave']."</td>
                            </tr>
                            <tr>
                                <td style='font-size:18px; padding-top:16px'>Vacation Leaves Remaining:</td>
                                <td style='font-size:18px'>".$row['vac_Leave']."</td>
                            </tr>
                            <tr>
                                <td style='font-size:18px; padding-top:16px'>Position:</td>
                                <td style='font-size:18px'>".$row['position_type']."</td>
                            </tr>
                            <tr>
                                <td style='font-size:18px; padding-top:16px'>PHIC: </td>
                                <td style='font-size:18px'>".$row['phic']."</td>
                            </tr>
                            <tr>
                                <td style='font-size:18px; padding-top:16px'>Pag-Ibig: </td>
                                <td style='font-size:18px'>".$row['pagibig']."</td>
                            </tr>
                            <tr>
                                <td style='font-size:18px; padding-top:16px'>SSS: </td>
                                <td style='font-size:18px'>".$row['sss']."</td>
                            </tr>
                            <tr>
                                <td style='font-size:18px; padding-top:16px'>Tin: </td>
                                <td style='font-size:18px'>".$row['tin']."</td>
                            </tr>
                            <tr>
                                <td style='font-size:18px; padding-top:16px'>Department: </td>
                                <td style='font-size:18px'>".$row['department']."</td>
                            </tr>
                            <tr>
                                <td style='font-size:18px; padding-top:16px'>Date of Employment: </td>
                                <td style='font-size:18px'>".$datehired->format('M-d-Y')."</td>
                            </tr>
                            <tr>
                                <td style='font-size:18px; padding-top:16px'>Quota: </td>
                                <td style='font-size:18px'>".$row['requiredamount']."</td>
                            </tr>

                        </table>
                        <hr>
                        <a href='viewuserprofile.php?id=".$_GET['id']."&edit=true'>
                            <input type='button'; value='Edit'; class='uk-button uk-button-primary'>
                        </a>";
                    }
        ?>
        </div> 
    </div>
</body>
</html>