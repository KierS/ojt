<?php
    session_start();
  include('connect.php');

  $output = '';
  if(isset($_POST["export_excel"]))
  {
      $sql = $_SESSION['SQL'];
      $result = mysqli_query($conn, $sql);

      if(mysqli_num_rows($result) > 0)
      {
          $output .= '
            <table class = "table" bordered = "1">
                <tr>
                    <th>ID Number</th>
                    <th>Last Name</th>
                    <th>First Name</th>
                    <th>Position</th>
                </tr>
          ';
          while($row = mysqli_fetch_array($result))
          {
              $output .= '
                <tr>
                    <td>'.$row["idnumber"].'</td>
                    <td>'.$row["Last_name"].'</td>
                    <td>'.$row["first_name"].'</td>
                    <td>'.$row["position_type"].'</td>
                </tr>
              ';
          }

          $output .= '</table>';
          header("Content-Type: application/xls");
          header("Content-Disposition: attachment; filename=ExcelTest.xls");
          echo $output;
      }
  }
?>
