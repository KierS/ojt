<?php

    session_start();

    error_reporting(E_PARSE | E_ERROR);

    if (!isset($_SESSION['logged_in'])) 
    {
        header("location: login.php");
    }
    else
    {
        include('connect.php');
    }

    $user = $_SESSION['user'];
    $idnumber = $_SESSION['idnumber'];

    $sql = "SELECT * FROM accounts WHERE idnumber = '$idnumber'";
    $sqlResult = mysqli_query($conn, $sql);
    $sqlRow = mysqli_fetch_array($sqlResult);

    $password  = $sqlRow['password'];

    $errors = array();


    if(isset($_POST['change']))
    {
        $currPass = $_POST['currPass'];
        $newPass1 = $_POST['newPass1'];
        $newPass2 = $_POST['newPass2'];

        if (empty($currPass))
        {
            array_push($errors, "Current Pass is required");
        }
        if (empty($newPass1))
        {
            array_push($errors, "New Password is required");
        }
        if (empty($newPass2))
        {
            array_push($errors, "Retype New Password");
        }
        if ($password != $currPass)
        {
            array_push($errors, "Current password is incorrect");
        }
        if ($newPass1 != $newPass2)
        {
            array_push($errors, "The passwords do not match");
        }

        if(count($errors) == 0)
        {
            $password = md5($newPass1);

            $sqlUp = "UPDATE accounts SET password = '$password' WHERE idnumber = '$idnumber'";
            mysqli_query($conn, $sqlUp);

            echo "SUCCESS!";
        }
    }
?>