<?php
    session_start();
    if (!isset($_SESSION['logged_in'])) 
    {
        header("location: login.php");    
    }
    else
    {
        if ($_SESSION['position'] == "Admin")
        {
            include("connect.php");
            include ("regServer.php");
        }
        else
        {
            header("location: sample.php");
        }
    }
?>
<html>
    <head>
        <title> TAS Tradesoft - Expense Report </title>
        <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="stylesheet" href="css/uikit.css" />
            <script src="js/uikit.min.js"></script>
            <script src="js/uikit-icons.min.js"></script>
            <script type="text/javascript" src="js/timeScripts.js"></script>
    </head>

    <body>
        <?php include("navbar.php"); ?>
        <div class="uk-card uk-card-medium uk-card-primary uk-position-center uk-width-1-6@m">
            <div class="uk-container uk-container-large">
                <div class="uk-card-header">
                    <h3 class="uk-card-title uk-margin-remove-bottom uk-text-center">Register Account</h3>
                </div>
                <!-- FORM HEAD -->
                <form method= "post" action="regForm2.php">
                    <div class="uk-card-body uk-text-center">
                        <!-- FORM BODY -->
                        <label class="uk-form-label" for="form-horizontal-text">Birthdate</label>
                            <div class="form-input ">
                                <input class="uk-input uk-form-width-medium uk-text-center" type="date" name="birthdate" /> 
                            </div>
                        <label class="uk-form-label" for="form-horizontal-text">Address</label>
                            <div class="form-input ">
                                <input class="uk-input uk-form-width-medium uk-text-center" type="text" name="homeaddress" /> 
                            </div>
                        <label class="uk-form-label" for="form-horizontal-text">Phil Health</label>
                            <div class="form-input ">
                                <input class="uk-input uk-form-width-medium uk-text-center" type="text" name="philhealth" /> 
                            </div>
                        <label class="uk-form-label" for="form-horizontal-text">Pag Ibig</label>
                            <div class="form-input ">
                                <input class="uk-input uk-form-width-medium uk-text-center" type="text" name="pagibig" /> 
                            </div>
                        <label class="uk-form-label" for="form-horizontal-text">SSS</label>
                            <div class="form-input ">
                                <input class="uk-input uk-form-width-medium uk-text-center" type="text" name="sss" /> 
                            </div>
                        <label class="uk-form-label" for="form-horizontal-text">Tin</label>
                            <div class="form-input ">
                                <input class="uk-input uk-form-width-medium uk-text-center" type="text" name="tin" /> 
                            </div>
                    </div>

                    <input type="hidden" value=" <?php echo $_POST['idnumber']; ?>" name = "idnumber">
                    <input type="hidden" value=" <?php echo $_POST['fName']; ?>" name = "fName">
                    <input type="hidden" value=" <?php echo $_POST['lName']; ?>" name = "lName">
                    <!-- FORM BODY END -->

                <!-- FORM SUBMIT -->
                <div class="uk-card-footer">
                    <button type= "submit" name="form2" class="uk-button uk-button-primary ">Next</button>
                </div>
                <!-- FORM SUBMIT -->
                </form>
            </div>
        </div>


    </body>
</html>
