<?php
    session_start();
    if (!isset($_SESSION['logged_in'])) 
    {
        header("location: login.php");    
    }
    else
    {
        if ($_SESSION['position'] == "Admin")
        {
            include("connect.php");
            include ("regServer.php");
        }
        else
        {
            header("location: sample.php");
        }
    }
?>
<html>
    <head>
        <title> TAS Tradesoft - Expense Report </title>
        <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="stylesheet" href="css/uikit.css" />
            <script src="js/uikit.min.js"></script>
            <script src="js/uikit-icons.min.js"></script>
            <script type="text/javascript" src="js/timeScripts.js"></script>
    </head>

    <body>
        <?php include("navbar.php"); ?>
        <div class="uk-card uk-card-medium uk-card-primary uk-position-center uk-width-1-6@m">
            <div class="uk-container uk-container-large">
                <div class="uk-card-header">
                    <h3 class="uk-card-title uk-margin-remove-bottom uk-text-center">Register Account</h3>
                </div>
   
                <form method= "post" action="regServer.php">
                    <div class="uk-card-body uk-text-center">
                        <!-- FORM BODY -->
                        <label class="uk-form-label" for="form-horizontal-text">Department</label>
                            <div class="form-input ">
                                <input class="uk-input uk-form-width-medium" type="text" name="department" /> 
                            </div>
                        <label class="uk-form-label" for="form-horizontal-text">Position</label>
                            <div  class="uk-margin">
                                <select name = "type" class="uk-select" class="uk-margin-small-bottom uk-margin-small-top uk-select uk-form-small">
                                    <option>Admin</option>
                                    <option>Employee</option>
                                </select>
                            </div>
                        <label class="uk-form-label" for="form-horizontal-text">Date Hired</label>
                            <div class="form-input ">
                                <input class="uk-input uk-form-width-medium" type="date" name="datehired" /> 
                            </div>
                        <label class="uk-form-label" for="form-horizontal-text">Quota</label>
                            <div class="form-input ">
                                <input class="uk-input uk-form-width-medium" type="text" name="requiredamount" /> 
                            </div>                            
                        <label class="uk-form-label" for="form-horizontal-text uk-text-center">Password</label>
                            <div class="form-input">
                                <input class="uk-input uk-form-width-medium"  type="password" name="password_1" />
                            </div>
                        </div>
                        <!-- FORM BODY END -->
                    </div>



                    <!-- HIDDEN VALUES -->
                    <input type="hidden" value=" <?php echo $_POST['idnumber']; ?>" name = "idnumber">
                    <input type="hidden" value=" <?php echo $_POST['fName']; ?>" name = "fName">
                    <input type="hidden" value=" <?php echo $_POST['lName']; ?>" name = "lName">
                    <input type="hidden" value=" <?php echo $_POST['birthdate']; ?>" name = "birthdate">
                    <input type="hidden" value=" <?php echo $_POST['homeaddress']; ?>" name = "homeaddress">
                    <input type="hidden" value=" <?php echo $_POST['pagibig']; ?>" name = "pagibig">
                    <input type="hidden" value=" <?php echo $_POST['philhealth']; ?>" name = "philhealth">
                    <input type="hidden" value=" <?php echo $_POST['sss']; ?>" name = "sss">
                    <input type="hidden" value=" <?php echo $_POST['tin']; ?>" name = "tin">
                    <!-- HIDDEN VALUES -->
                    
                <!-- FORM SUBMIT -->
                <div class="uk-card-footer">
                    <button type= "submit" name="register" class="uk-button uk-button-primary" onclick = "return mess()">Next</button>
                </div>
                <!-- FORM SUBMIT -->
                </form>
            </div>
        </div>

        
        <script type = "text/javascript">
                function mess()
                {
                    alert ("New User Added!");
                    return true;
                }
        </script>   
    </body>
</html>
