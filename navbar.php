<?php

if($_SESSION['position'] == "Admin" )
{
    echo "
            <nav class='uk-navbar-container uk-margin' uk-navbar style='margin-bottom:0px;'><!---->
                <div class='uk-navbar-left'>
                    <div>
                        <ul class='uk-navbar-nav'>
                            <li>
                                <img src='TAS1.png' style= 'width:100px; height:40px; margin-top:20px' alt=''> 
                            </li>
                            <li>
                                <a href='sample.php' uk-icon='icon: album; ratio: 1.6'>Receipt Manager</a>
                            </li>

                            <li>
                                <a href='userlistpage.php'  uk-icon='icon: user; ratio: 1.8'>User List</a>
                            </li>  
                            <li>
                                <a href='leaveManager.php' uk-icon='icon: future; ratio: 1.6'>Leave Manager</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class='uk-navbar-right'>
                        <ul class='uk-navbar-nav' action ='logout'>
                        <li>
                            <a href='editPass.php' uk-icon='icon: cog; ratio: 1.6'>Change Password</a>
                        </li>
                        <li>
                            <a href='regForm.php' uk-icon='icon: plus-circle; ration: 2.2'>Add User</a>
                        </li>
    
                        <li>
                            <a href='logout.php' uk-icon='icon: sign-out; ratio: 2.2'>Log-Out</a>
                        </li>
                    </ul>
                </div>
            </nav>
        ";
    }
    else if($_SESSION['position'] == "Employee")
    {
        echo "
        <nav class='uk-navbar-container uk-margin' uk-navbar style='margin-bottom:0px;'><!---->
            <div class='uk-navbar-left'>
                <div>
                    <ul class='uk-navbar-nav'>
                        <li>
                                <img src='TAS1.png' style= 'width:100px; height:40px; margin-top:20px' alt=''> 
                        </li>
                        <li>
                            <a href='sample.php' uk-icon='icon: album; ratio: 1.6'>Receipt Manager</a>
                        </li>
                        <li>
                            <a href='leaveApplication.php' uk-icon='future'>Apply Leave</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class='uk-navbar-right'>
                <ul class='uk-navbar-nav' action ='logout'>
                    <li>
                        <a href='editPass.php' uk-icon='icon: cog; ratio: 1.6'>Change Password</a>
                    </li>
                    <li>
                        <a href='logout.php' uk-icon='icon: sign-out;'>Log-Out</a>
                    </li>
                </ul>
            </div>
        </nav>
    ";
    }
?>