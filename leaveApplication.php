<?php

    session_start();
    
    if(!isset($_SESSION['logged_in']))
    {
        header("location: login.php");
    }  
    else
    {
        include('connect.php');
        include('applyLeave.php'); 

        $position = $_SESSION['position'];
        $user = $_SESSION['user'];
    }

    #Allows display of table variable
    $sql = "SELECT * FROM accounts WHERE user = '$user'";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_array($result);

    #echo $row['sick_Leave']."<br>". $row['vac_Leave'];
?>

<!DOCTYPE html>
<html>
    <head>
        <title> TAS Tradesoft - Expense Report </title>
        <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="stylesheet" href="css/uikit.css" />
            <script src="js/uikit.min.js"></script>
            <script src="js/uikit-icons.min.js"></script>
            <script type="text/javascript" src="js/timeScripts.js"></script>
    </head>

    <body onLoad = "renderTime();">
    <?php
        include("navbar.php");
    ?>
            <div class="uk-card uk-card-small uk-card-secondary uk-position-center uk-width-1-5@m">
                <div class="uk-container uk-container-small">
                    <div class="uk-card-header">
                        <h3 class="uk-card-title uk-margin-remove-bottom uk-text-center">Leave Application</h3>
                    </div>

                    <form method= "post" action="leaveApplication.php" id="leaveApp">
                        <div class="uk-card-body uk-text-center">
                            <label class="uk-form-label" for="form-horizontal-text">Employee</label>
                                <div class="form-input">
                                    <input class="uk-input uk-form-width-medium uk-text-center" type="text"; name="user"; value = "<?php echo $user;?>"; readonly="<?php echo $user;?>";/>
                                </div>
                                
                            <label class="uk-form-label" for="form-horizontal-text">From Date</label>
                                <div class="form-input ">
                                    <input class="uk-input uk-form-width-medium" type="date" name="fromDate" /> 
                                </div>

                            <label class="uk-form-label" for="form-horizontal-text" uk-text uk-text-center>To Date</label>
                                <div class="form-input ">
                                    <input class="uk-input uk-form-width-medium" type="date" name="toDate" /> 
                                </div>

                            <label class="uk-form-label" for = "form-horizontal-text" uk-text uk-text-center>Type of Leave</label>
                            <div class="uk-margin uk-grid-small uk-child-width-auto form-input">
                                <label><input class="uk-radio" type="radio" name="lType"; value = "Vacation" checked> Vacation <?php echo " (".$row['vac_Leave']." left) "?> </label>
                                <label><input class="uk-radio" type="radio" name="lType"; value = "Sick" > Sick <?php echo " (".$row['sick_Leave']." left)"?> </label>
                            </div>

                            <label class="uk-form-label" for="form-horizontal-text">Reason for Leave</label>
                                <div class="form-input ">
                                    <textarea row="10" col="30" name="description" placeholder="Description (Max 100 characters)" maxlength="100" form="leaveApp"></textarea>
                                </div>                         
                        </div>

                        <div class="uk-card-footer">
                            <button type= "submit" name="apply" class="uk-button uk-button-secondary" onclick = "return mess()">Send Application</button>
                        </div>
                    </form>
                </div>
            </div>
            
            <script type = "text/javascript">
                function mess()
                {
                    alert ("Leave Application Sent!");
                    return true;
                }
            </script>   
    </body>
</html> 