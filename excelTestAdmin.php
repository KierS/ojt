<?php

session_start();

$queries = $_SESSION['query'];
//$employ = $_SESSION['employ'];
$sqlgross = $_SESSION['sqlgross'];
$sqlvat = $_SESSION['sqlvat'];
$sqlvatable = $_SESSION['sqlvatable'] ;
$sqlnonvat = $_SESSION['sqlnonvat'];

require_once 'Classes/PHPExcel.php';

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "tas_receipt";

$conn = new mysqli($servername, $username, $password, $dbname);

$TODAY = date('M-d-Y');

#creating PHPExcel object------------------------------------------
$excel = new PHPExcel();
$objDrawing = new PHPExcel_Worksheet_Drawing();


#Adding image to EXCEL
$objDrawing->setName('Logo');
$objDrawing->setDescription('Logo');
$objDrawing->setPath('./Classes/TAS1.png');
$objDrawing->setCoordinates('D1');
$objDrawing->setOffsetX(0);
$objDrawing->setRotation(0);
$objDrawing->setHeight(50);
$objDrawing->setWidthAndHeight(250,74);
$objDrawing->setResizeProportional(true);
$objDrawing->getShadow()->setVisible(true);
$objDrawing->getShadow()->setDirection(45);
$objDrawing->setWorksheet($excel->getActiveSheet());


#Code for Filling up the Cell Color--------------------------------
function cellColor($cells,$color){
    global $excel;

    $excel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => $color
        )
    ));
}

//cellColor('A8', 'F28A8C');-----single cell coloring
cellColor('A8:K8', '0066ff');//-----costum cell coloring
cellColor('N8:N11','32CD32');

#FONT COLORING-------------------------------------------------------
	$excel->getActiveSheet()->getStyle('A8:K8')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
	#$excel->getActiveSheet()->getStyle('K8:K11')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);



	#insert some data to PHPExcel object-------------------------------
	$excel->setActiveSheetIndex(0);

#Populate the data--------------------------------------------------



$userEmployee=$_SESSION['user'];
$sql2 = "SELECT * FROM accounts WHERE user = '$userEmployee'";
$sql2result = mysqli_query($conn,$sql2);
$rhow = mysqli_fetch_array($sql2result);

$sql3 = "SELECT * FROM accounts WHERE user = '$userEmployee'";
$sql3result = mysqli_query($conn,$sql3);
$rhow1 = mysqli_fetch_array($sql3result);
$pos = $rhow1['position_type'];
$id = $rhow1['idnumber'];
$dept = $rhow1['department'];

$sql = "SELECT * FROM receipt ORDER BY DATE(date) ASC";
$sqlresult = mysqli_query($conn,$sql);
$row = mysqli_fetch_array($sqlresult);

$totalAmount = $_SESSION['totalAmount'];
$totalVatAmount = $_SESSION['totalVatAmount'];
$totalNonVat = $_SESSION['totalNonVat'];
$totalVatableAmount = $_SESSION['totalVatableAmount'];

$query = mysqli_query($conn,$queries);

$sql01="SELECT sum(amount) AS total FROM receipt WHERE MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE())";
$sql02="SELECT sum(vatAmount) AS total1 FROM receipt WHERE MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE())";//net
$sql03="SELECT sum(nonVat) AS total2 FROM receipt WHERE MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE())";
$sql04="SELECT sum(vatableAmount) AS total3 FROM receipt WHERE MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE())";

// if ($result1=mysqli_query($conn,$sqlgross))
//       {
//         while($rhow1 = mysqli_fetch_array($result1))
//         {
//         	$amount1 = $rhow1['total'];#output of TOTAL AMOUNT/GROSS is on the line 179.
//     	}
//       }

// if ($result2=mysqli_query($conn,$sqlvat))
//       {
//         while($rhow2 = mysqli_fetch_array($result2))
//         {
//         $amount2 = $rhow2['total1'];
//           #.number_format($amount2,2).output of TOTAL NET is on the line 180
//         }
//       }

// if ($result3=mysqli_query($conn,$sqlnonvat))
//       {
//         while($rhow3 = mysqli_fetch_array($result3))
//         {
//         $amount3 = $rhow3['total3'];
//           #.number_format($amount3,2).output of TOTAL NON VATABLES is on the line 181
//         }
//       }

// if ($result4=mysqli_query($conn,$sqlvatable))
//       {
//         while($rhow4 = mysqli_fetch_array($result4))
//         {
//         $amount4 = $rhow4['total2'];
//           #.number_format($amount4,2).output of TOTAL VATABLES is on the line 182
//         }
//       }
   


    $row = 9;
    $sn = 1;


while($data = mysqli_fetch_object($query)){
	$excel->getActiveSheet()

		//->setCellValue('A' .$row , $data->sn)
		//->setCellValue('A' .$row , ''.$sn)
		->setCellValue('B' .$row , $data->date)
		->setCellValue('C' .$row , $data->storename)
		->setCellValue('D' .$row , $data->type)
		->setCellValue('E' .$row , $data->amount)
		->setCellValue('F' .$row , $data->vat)
		->setCellValue('G' .$row , $data->vatAmount)
		->setCellValue('H' .$row , $data->nonVat)
		->setCellValue('I' .$row , $data->vatableAmount)
		->setCellValue('J' .$row , $data->tin)
		->setCellValue('K' .$row , $data->address);


	#increment the row	
	$row++;
	$sn++;
}
#set column width----------------------------------------------------
$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$excel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
$excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
#$excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
$excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);


#make table headers
$excel->getActiveSheet()//Column Title of Data on SQL DATABASE
	->setCellValue('A1','Expense Report')
	->setCellValue('A5','Name:')
	->setCellValue('B5',$userEmployee)	b
	->setCellValue('A4','EMPLOYEE INFORMATION:')
	->setCellValue('A6','Department/Division: '.$dept)
	->setCellValue('D4','Description:')
	->setCellValue('D5','Position: '.$pos)

	->setCellValue('D6','Amount:')
	->setCellValue('H2','Month:')
	->setCellValue('H3','Date: '.$TODAY)
	->setCellValue('H5','SSN:')
	->setCellValue('H6','Employee ID: '.$id)
	->setCellValue('A8','SN')
	->setCellValue('B8', 'DATE')
	->setCellValue('C8', 'STORENAME')
	->setCellValue('D8', 'TYPE')
	->setCellValue('E8', 'AMOUNT')
	->setCellValue('F8', 'VAT')
	->setCellValue('G8', 'NET')
	->setCellValue('H8', 'NON VATABLES')
	->setCellValue('I8', 'VATABLES')
	->setCellValue('J8', 'TIN')
	->setCellValue('K8', 'Address')

	->setCellValue('M8','Amount(total):')
	->setCellValue('M9','Net(total):')
	->setCellValue('M10','Non Vatables(total):')
	->setCellValue('M11','Vatables(total):')
    ->setCellValue('N8', number_format($totalAmount,2))
	->setCellValue('N9', number_format($totalVatAmount,2))
	->setCellValue('N10',number_format($totalNonVat,2))
	->setCellValue('N11',number_format($totalVatableAmount,2))

	->setCellValue('M13','PREPARED BY:')
	->setCellValue('M16','Jinky Cango')
	->setCellValue('M17','Admin');
	$excel->getActiveSheet()->getStyle('M16')->getFont()->setUnderline(true);
	$excel->getActiveSheet()->getStyle('M16')->getFont()->setItalic(true);


	

#merging the title
$excel->getActiveSheet()->mergeCells('A4:B4');
$excel->getActiveSheet()->mergeCells('A6:B6');
$excel->getActiveSheet()->mergeCells('A1:C1');
$excel->getActiveSheet()->mergeCells('D1:E3');
#BOLD TEXT
$excel->getActiveSheet()->getStyle('A4:D4')->getFont()->setBold(true);
$excel->getActiveSheet()->getStyle('A5')->getFont()->setBold(true);
$excel->getActiveSheet()->getStyle('A6:B6')->getFont()->setBold(true);
$excel->getActiveSheet()->getStyle('A4:C4')->getFont()->setBold(true);
$excel->getActiveSheet()->getStyle('D5:D6')->getFont()->setBold(true);
$excel->getActiveSheet()->getStyle('H6')->getFont()->setBold(true);
$excel->getActiveSheet()->getStyle('H2')->getFont()->setBold(true);
$excel->getActiveSheet()->getStyle('H3')->getFont()->setBold(true);
$excel->getActiveSheet()->getStyle('H5')->getFont()->setBold(true);

#aligning
$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal('center');

#styling
$excel->getActiveSheet()->getStyle('A1')->applyFromArray(
	array(
			'font'=>array(
				'size'=>24,
			)
		)
);
$excel->getActiveSheet()->getStyle('A8:K8')->applyFromArray(
	array(
			'font'=>array(
				'bold'=>true
			),
			'borders'=>array(
				'allborders'=>array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		)
);
$excel->getActiveSheet()->getStyle('A8:K'.($row-1))->applyFromArray(
	array(
		'borders'=>array(
			'outline'=>array(
				'style'=>PHPExcel_Style_Border::BORDER_THIN
			),
			'vertical'=>array(
				'style'=>PHPExcel_Style_Border::BORDER_THIN)
		)
	)
);
$excel->getActiveSheet()->getStyle('M8:M11')->applyFromArray(
	array(
			'font'=>array(
				'bold'=>true
			),
			'borders'=>array(
				'allborders'=>array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		)
);
$excel->getActiveSheet()->getStyle('N8:N11')->applyFromArray(
	array(
			'font'=>array(
				'bold'=>true
			),
			'borders'=>array(
				'allborders'=>array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		)
);

//redirect to browser (download) instead of saving the result as a file
//this is for MS Office Excel xls format
header('Content-Type:application/vnd.opemxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition:attachment; filename="test.xls"');

//write the result to a file
$file = PHPExcel_IOFactory::createWriter($excel,'Excel2007');
//output to php output instead of filename
$file->save('php://output');

?>