<?php
session_start();

include("connect.php")

if($_SESSION['position'] != "Admin" || $_SESSION['position'] != "admin")
{
    header("location: sample.php");
} 

?>
<html>
<head>
<script>

function printReceipt(theDiv)
{
    var restorePage = document.body.innerHTML;
    var printContent = document.getElementById(theDiv).innerHTML;
    document.body.innerHTML = printContent;
    window.print();
    document.body.innerHTML = restorePage;
}

</script>
    
    <title> TAS Tradesoft - Leave Report </title>
    <meta charset="utf-8">
            <meta   name="viewport" content="width=device-width, initial-scale=1">
            <link   rel="stylesheet" href="css/uikit.css" />
            <script src="js/uikit.min.js"></script>
            <script src="js/uikit-icons.min.js"></script>
</head>
<body style="background-color:#cccccc">
<?php

    $sql= "SELECT * FROM receipt WHERE sn = '" . $_GET['sn'] . "'";
    echo $sql;


    $result = mysqli_query($conn,$sql);

    $row = mysqli_fetch_array($result);
    include("navbar.php");

    $tin = $row['tin'];
    $vat = $row['vat'];
    $TODAY= date("Y-m-d");
    $TIME = date("H:i:s"); 
    $type = $row['type'];
    $store = $row['storename'];
    $amount = $row['amount'];
    $total = $row['amount'];
    $VATAmount = $row['vatableAmount'];
    $VATExempt = $row['nonVat'];
    $VATSales = $row['vatAmount'];
    $VATValue = $row['vat'] * 100 . "% VAT";

    // $tin = "###-###-###-###";
    // $vat = "###########";
    $receiptNum = "###########"; 

    // $type = "PLACEHOLDER";
    // $store = "PLACEHOLDER ";
    // $amount = "####.##";
    // $total = "####.##";
    $cash = "####.##";
    $tRend = "####.##";
    $change = "####.##";
    // $VATAmount = "####.## ";
    // $VATExempt = "####.##";
    // $VATSales = "####.##";
    // $VATValue = "##% VAT";
?>
<div id="container"; style="text-align:center; ">
    <div id="theReceipt" style = "background-color:white; width: 50%; height:100%; margin:auto">
    <br>
        <div id = "buttoni"; style="border: 1px solid black ; width:200px; height:auto; margin:auto; background-color:white">
            <div> 
                <br>
                <table style="width:100%">
                    <tr>
                        <td colspan="3"; style="text-align:center"><img src="TAS1.png"; style="width:70px; height:auto; padding-bottom:10px">
                    </tr>

                    <tr>
                        <td colspan="3"; style="text-align:center"><p style="font-size:12px;margin:0;padding:0"> TAS Tradesoft Corporation Bacolod
                        </tr> </td>
                        <td colspan="3"; style="text-align:center"><p style="font-size:8px; margin:0;padding:0"> TAS Tradesoft Corp.
                        </tr>
                    <tr>
                        <td colspan="1"; style="text-align:left"><p style="font-size:8px; margin:0;padding:0">TIN <?php echo " ".$tin." " ?> </p>
                        <td colspan="2"; style="text-align:right"><p style="font-size:8px; margin:0;padding:0;">VAT <?php echo " ".$vat." " ?></p>
                    </tr>
                    <tr>
                        <td colspan="3"; style="text-align:center"><p style="font-size:8px; margin:0;padding:0">Door 5 MK5/5A, Art District Bldg.
                    </tr>
                    <tr>
                        <td colspan="3"; style="text-align:center"><p style="font-size:8px; margin:0;padding:0">PERMIT # ####-###-######-###
                    </tr>
                    <tr>
                        <td><p style="font-size:8px; margin:0;padding:0;">Terminal #: ### </td>
                        <td colspan="2"; style="text-align:right"><p style="font-size:8px; margin:0;padding:0;">Receipt #: <?php echo $receiptNum ?> </td>
                    </tr>
                    <tr>
                        <td colspan="1"; style="text-align:left"><p style="font-size:8px; margin:0;padding:0"><?php echo $TODAY; ?></td>
                        <td colspan="2"; style="text-align:right"><p style="font-size:8px; margin:0;padding:0"><?php echo $TIME; ?></td>
                    </tr>
                    <!-- TABLE FOR ITEMS -->
                </table>
                <hr style="margin:1; padding:0; border: 1px dashed gray">
                <table  style="width:100%">
                <tr>
                    <th style="font-size:8px; text-align:center">TYPE</th>
                    <th style="font-size:8px; text-align:center">STORE</th>
                    <th style="font-size:8px; text-align:center">AMOUNT</th>
                </tr>
                <tr>
                    <td style= "font-size:8px;text-align:left"><?php echo $type;  ?></td>
                    <td style= "font-size:8px;text-align:center"><?php echo $store; ?></td>
                    <td style= "font-size:8px;text-align:right"><?php echo $amount;?></td>
                </tr>
                </table> 
                <hr style="margin:2; padding:0; border: 1px dashed gray">
                <table  style="width:100%">
                <tr>
                    <td style= "font-size:8px; text-align:left">TOTAL</td>
                    <td style= "font-size:8px; text-align:right"><?php echo $total; ?></td>
                </tr>
                <tr>
                    <td style= "font-size:8px; text-align:left">CASH</td>
                    <td style= "font-size:8px; text-align:right"><?php echo $cash; ?></td>
                </tr>
                <tr>
                    <td style= "font-size:8px; text-align:left">TOTAL TENDERED</td>
                    <td style= "font-size:8px; text-align:right"><?php echo $tRend; ?></td>
                </tr>
                <tr>
                    <td style= "font-size:8px; text-align:left">CHANGE</td>
                    <td style= "font-size:8px; text-align:right"><?php echo $change; ?></td>
                </tr>
                </table> 
                <hr style="margin:2; padding:0; border: 1px dashed gray">
                <table style="width:100%">
                <tr>
                    <td style= "width: 30%; font-size:8px; text-align:left">VAT SALES</td>
                    <td style= "font-size:8px; text-align:left"> : </td>
                    <td style= "font-size:8px; text-align:left"><?php echo $VATSales; ?></td>
                </tr>
                <tr>
                    <td style= "font-size:8px; text-align:left"><?php echo $VATValue ?></td>
                    <td style= "font-size:8px; text-align:left"> : </td>
                    <td style= "font-size:8px; text-align:left"><?php echo $VATAmount; ?></td>
                </tr>
                <tr>
                    <td style= "font-size:8px; text-align:left">VAT EXEMPT</td>
                    <td style= "font-size:8px; text-align:left"> : </td>
                    <td style= "font-size:8px; text-align:left"><?php echo $VATExempt; ?></td>
                </tr>
                </table>
            </div>
        </div>
        </div>
        <button onclick="printReceipt('theReceipt')"; class="uk-button uk-button-primary"; style="width:25%; margin-top:10px;font-size:12px">PRINT RECEIPT</button>
    
    
</div>
</body>
</html>