<?php 
 
session_start();
 //echo !isset($_SESSION['logged_in']) ;die();
 if (!isset($_SESSION['logged_in'])) {//============================
  header("location: login.php");    //==============================
}

else{ 

 include('connect.php');

 include ('server.php');
}
?>


<!DOCTYPE html>
<html>
<head>


 <title> TAS TRADESOFT - Expense Report</title>



 <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/uikit.css" />
        <script src="js/uikit.min.js"></script>
        <script src="js/uikit-icons.min.js"></script>
 
</head>
<body>
    <?php   include ('errors.php');?>

    <nav class="uk-navbar-container uk-margin" uk-navbar>
    <div class="uk-navbar-left">

        <ul class="uk-navbar-nav">
            <li>
                <a href="admin.php">
                    TAS Tradesoft Expense Report
                </a>
            </li>
        </ul>

        
    </div>

    
</nav>
    <div class="uk-card uk-card-medium uk-card-primary uk-position-center uk-width-1-6@m">
        <div class="uk-container uk-container-large">
            <div class="uk-card-header">
                <h3 class="uk-card-title uk-margin-remove-bottom">Register Account</h3>
            </div>
            <form method= "post" action="register.php">
                <div class="uk-card-body">
                    <label class="uk-form-label" for="form-horizontal-text">ID Number</label>
                    <div class="form-input ">
                        <input class="uk-input uk-form-width-medium" type="text" name="idnumber" /> 
                    </div>
                    <label class="uk-form-label" for="form-horizontal-text">First Name</label>
                    <div class="form-input ">
                        <input class="uk-input uk-form-width-medium" type="text" name="firstname" /> 
                    </div>
                    <label class="uk-form-label" for="form-horizontal-text">Last Name</label>
                        <div class="form-input ">
                            <input class="uk-input uk-form-width-medium" type="text" name="lastname" /> 
                        </div>
                    <label class="uk-form-label" for="form-horizontal-text">Birthdate</label>
                        <div class="form-input ">
                            <input class="uk-input uk-form-width-medium" type="date" name="birthdate" /> 
                        </div>
                    <!-- <label class="uk-form-label" for="form-horizontal-text">Address</label>
                        <div class="form-input ">
                            <input class="uk-input uk-form-width-medium" type="text" name="homeaddress" /> 
                        </div>
                    <label class="uk-form-label" for="form-horizontal-text">Phil Health</label>
                        <div class="form-input ">
                            <input class="uk-input uk-form-width-medium" type="text" name="philhealth" /> 
                        </div>
                    <label class="uk-form-label" for="form-horizontal-text">Pag Ibig</label>
                        <div class="form-input ">
                            <input class="uk-input uk-form-width-medium" type="text" name="pagibig" /> 
                        </div>
                    <label class="uk-form-label" for="form-horizontal-text">SSS</label>
                        <div class="form-input ">
                            <input class="uk-input uk-form-width-medium" type="text" name="sss" /> 
                        </div>
                    <label class="uk-form-label" for="form-horizontal-text">Tin</label>
                        <div class="form-input ">
                            <input class="uk-input uk-form-width-medium" type="text" name="tin" /> 
                        </div> -->
                    <label class="uk-form-label" for="form-horizontal-text">Department</label>
                        <div class="form-input ">
                            <input class="uk-input uk-form-width-medium" type="text" name="department" /> 
                        </div>
                    <label class="uk-form-label" for="form-horizontal-text">Position</label>
                        <div  class="uk-margin">
                            <select name = "type" class="uk-select" class="uk-margin-small-bottom uk-margin-small-top uk-select uk-form-small">
                                <option>Admin</option>
                                <option>Employee</option>
                            </select>
                        </div>
                    <label class="uk-form-label" for="form-horizontal-text">Date Hired</label>
                        <div class="form-input ">
                            <input class="uk-input uk-form-width-medium" type="date" name="datehired" /> 
                        </div>
                    <label class="uk-form-label" for="form-horizontal-text">Quota</label>
                        <div class="form-input ">
                            <input class="uk-input uk-form-width-medium" type="text" name="requiredamount" /> 
                        </div>                            
                    <label class="uk-form-label" for="form-horizontal-text">Password</label>
                        <div class="form-input">
                            <input class="uk-input uk-form-width-medium"  type="password" name="password_1" />
                        </div>
                    <label class="uk-form-label" for="form-horizontal-text">Confirm Password</label>
                        <div class="form-input">
                            <input class="uk-input uk-form-width-medium"  type="password" name="password_2" />
                        </div>
                </div>     
                <div class="uk-card-footer">
                    <button type= "submit" name="register" class="uk-button uk-button-primary ">Register</button>
                </div>
        </form>
    </div>
</div>    
</body>
</html>