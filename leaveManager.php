<?php
    session_start();
	include ('connect.php');
	
    if ($conn->connect_error) 
    {
    	die("Connection failed: " . $conn->connect_error);
    } 

    if($_SESSION['position'] == "Employee" || $_SESSION['position'] == "employee")
    {
        header("location: sample.php");
    } 
    
    if(isset($_POST['search']))
    {
        $conditions = array();
        $searchSQL = "SELECT * FROM leave_tb";

        $filter = $_POST['filter'];
        $employee = $_POST['employee'];

        if (!empty($filter))
        {
            $conditions[] = "approval = $filter";  
        }   

        #allows the searching of ALL once SEARCH function has been operated.
        else if ($filter == "") 
        {
            
        }
        #allows the searching of PENDING queries. 
        else if ($filter == 0)
        {
            $conditions[] = "approval = $filter";  
        }

        if (!empty($employee))
        {
            $conditions[] = "user = '$employee'";
        }

        #if conditions are not empty // contents are greater than 0  
        if (count($conditions) > 0)
        {
            $searchSQL .= " WHERE " . implode(' AND ',$conditions);
        }
        $searchSQL .= " ORDER BY startDate ASC";
        $displayButton = "  <div class = 'uk-inline'>
                                <input type = 'submit' value = 'Hide Processed' class = 'uk-input uk-button-primary uk-form-small uk-width-1-1' name = 'unhide'><br/>     
                            </div>"; 
    } 

    else if(isset($_POST['hide']))
    {
        $searchSQL = "SELECT * FROM leave_tb WHERE approval < 3"; 
        $displayButton = "  <div class = 'uk-inline'>
                                <input type = 'submit' value = 'Show Processed' class = 'uk-input uk-button-primary uk-form-small uk-width-1-1' name = 'unhide'><br/>     
                            </div>"; 
        
    } 

    else if(isset($_POST['unhide']))
    {
        $searchSQL = "SELECT * FROM leave_tb ORDER BY startDate ASC"; 
        $displayButton = "  <div class = 'uk-inline'>
                                <input type = 'submit' value = 'Hide Processed' class = 'uk-input uk-button-primary uk-form-small uk-width-1-1' name = 'hide'><br/>     
                            </div>"; 
    } 

    else 
    {
       $searchSQL = "SELECT * FROM leave_tb ORDER BY startDate ASC"; 
       $displayButton = "   <div class = 'uk-inline'>
                                <input type = 'submit' value = 'Hide Processed' class = 'uk-input uk-button-primary uk-form-small uk-width-1-1' name = 'hide'><br/>     
                            </div>"; 
    } 
    $TODAY = date('Y-m-d H:i:s'); 

    #ON GOING
    $updateOSQL = "UPDATE leave_tb SET approval = 3 WHERE startDate <= '$TODAY' AND end_Date >= '$TODAY'";
    mysqli_query($conn, $updateOSQL);
    #echo $updateOSQL. "<br>";

    #COMPLETED
    $updateCSQL = "UPDATE leave_tb SET approval = 4 WHERE end_Date <= '$TODAY'";
    mysqli_query($conn, $updateCSQL);
    #echo $updateCSQL;
    
    $sqll = "SELECT * FROM accounts WHERE idnumber > 0 ORDER BY last_name";
?>
 
<html>
<head>
    
    <title> TAS Tradesoft - Leave Report </title>
    <meta charset="utf-8">
            <meta   name="viewport" content="width=device-width, initial-scale=1">
            <link   rel="stylesheet" href="css/uikit.css" />
            <script src="js/uikit.min.js"></script>
            <script src="js/uikit-icons.min.js"></script>
</head>
<body>
<?php
    include("navbar.php");
?>
<!-- Filter View -->
<form action="leaveManager.php" method ="post" style = "margin-bottom: 0px;" autocomplete="off">
    <div style="padding-left: 10px; padding-top:10px">
        <div class="uk-inline">
            <div class="uk-margin">
                <label class="uk-form-label">Filter:</label>
                <select class="uk-select uk-form-small" style="min-width:150px" name ="filter">
                <?php
                    echo "  <option selected value = ''>  ALL         </option>
                            <option value='0'>            Pending     </option>
                            <option value='1'>            Approved    </option>
                            <option value='2'>            Declined    </option>
                            <option value='3'>            On Going    </option>
                            <option value='4'>            Completed   </option>
                        ";
                ?>
                </select> 
            </div>
        </div>
        <div class="uk-inline">
            <div class="uk-margin">
                <label class="uk-form-label">Employee</label>
                <select class="uk-select uk-form-small" style="min-width:200px" name ="employee">
                <?php
                    if($resultl = mysqli_query($conn,$sqll))
                    {
                        echo "<option value=''>ALL</option>";
                        while($rowl = mysqli_fetch_array($resultl))
                        {
                            echo "<option value='".$rowl['first_name']." ".$rowl['last_name']."'>".$rowl['last_name']. ", ". $rowl['first_name']."</option>";
                        }
                    }
                ?>
                </select> 
            </div>
        </div>
        <div class="uk-inline">
            <input type = "submit" value = "SEARCH" class="uk-input uk-button-primary uk-form-small uk-width-1-1" name = "search"><br/>
        </div>
        <?php
            echo $displayButton;
        ?>
    </div> 
</form>

<!-- Filter View -->
<div class= "uk-overflow-auto">
    <p class = "uk-textarea uk-padding-remove uk-divider"; style="max-width: 100%; max-height: 100%; padding:10px"; >
    <?php
        include("leaveList.php");   
    ?>
    </p>
</div>
</body>
