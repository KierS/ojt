<?php

    session_start();

    error_reporting(E_PARSE | E_ERROR);

    if (!isset($_SESSION['logged_in'])) 
    {
        header("location: login.php");
    }

    include ('connect.php');

    $user = $_SESSION['user'];

    $sql = "SELECT * FROM accounts WHERE user = '$user'";
    $sqlResult = mysqli_query($conn, $sql);
    $sqlRow = mysqli_fetch_array($sqlResult);

    $id = $sqlRow['idnumber'];
    $pass = $sqlRow['password'];

    if(isset($_POST['changePass']))
    {
        $currentPass = md5($_POST['currPass']);
        $newPass1 = $_POST['newPass1'];
        $newPass2 = $_POST['newPass2'];

        if($currentPass == $pass)
        {
            if($newPass1 == $newPass2)
            {
                $password = md5($newPass1);

                $sqlUpdate = "UPDATE accounts SET password = '$password' WHERE idnumber = '$id'";
                mysqli_query($conn, $sqlUpdate);
                echo "<div class='uk-alert-success' uk-alert><a class='uk-alert-close' uk-close></a>SUCCESS!</div>";
            }
            else
            {
                echo "<br>NEW PASSWORDS DO NOT MATCH!";
            }
        }
        else
        {
            echo "<br>FALSE";
        }
    }

?>

<html>
    <head>
        <title> TAS Tradesoft - Expense Report </title>
        <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="stylesheet" href="css/uikit.css" />
            <script src="js/uikit.min.js"></script>
            <script src="js/uikit-icons.min.js"></script>
            <script type="text/javascript" src="js/timeScripts.js"></script>
    </head>

    <body>
    <?php include("navbar.php"); ?>

        </nav>

        <div class="uk-card uk-card-small uk-card-secondary uk-position-center uk-width-1-6@m">
                <div class="uk-container uk-container-small">
                    <div class="uk-card-header">
                        <h3 class="uk-card-title uk-margin-remove-bottom uk-text-center">Edit Password</h3>
                    </div>

                    <form method= "post" action="editPass.php">
                        <div class="uk-card-body uk-text-center">
                            <label class="uk-form-label" for="form-horizontal-text">Current Password</label>
                                <div class="form-input">
                                    <input class="uk-input uk-form-width-medium uk-text-center" type="password"; name="currPass";/>
                                </div>
                                
                            <label class="uk-form-label" for="form-horizontal-text">New Password</label>
                                <div class="form-input ">
                                    <input class="uk-input uk-form-width-medium" type="password" name="newPass1" /> 
                                </div>

                            <label class="uk-form-label" for="form-horizontal-text" uk-text uk-text-center>Retype Password</label>
                                <div class="form-input ">
                                    <input class="uk-input uk-form-width-medium" type="password" name="newPass2" /> 
                                </div>
                     
                        </div>

                        <div class="uk-card-footer">
                            <button type= "submit" name="changePass" class="uk-button uk-button-secondary"> Reset Password </button>
                        </div>
                    </form>
                </div>
            </div>
    </body>
</html>