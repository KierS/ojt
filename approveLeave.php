<?php

session_start();
	include ('connect.php');
	
    if ($conn->connect_error) 
    {
    	die("Connection failed: " . $conn->connect_error);
    } 

    else
    {
        
        include('connect.php');

        #Gets the ID from the Address Bar (URL)
        $id = $_GET['id'];

        $sql1 = "SELECT * from leave_tb WHERE leave_ID =  '$id'";
        $result1 = mysqli_query($conn, $sql1);
        $row1 = mysqli_fetch_array($result1);
        

        $type = $row1['leaveType'];
        $fromDate = $row1['startDate'];
        $toDate = $row1['end_Date'];
        $user = $row1['user'];
        $flag = $row1['changeFlag'];
        $condition = $row1['approval'];
      
        #echo $sql1."<br>".$type. "<br>". $fromDate. "<br>". $toDate;

        #Changes the condition to Approved... 1 = Approved, 0 = Pending, 2 = Declined etc...     
        $sql = "UPDATE leave_tb SET approval = 1 WHERE leave_ID = $id";
        $result = mysqli_query($conn, $sql);

        #Calculates the duration in days  of the leave
        $hfromDate = strtotime($fromDate);
        $htoDate = strtotime($toDate);
        $duration = $htoDate - $hfromDate;
        $duration = round($duration/(60*60*24));

        if($condition == "2" || $condition == "0")
        {
            #Determines either to deduct in Sick Leave or Vacation Leave
            if($type == "Sick" || $type == "sick" )
            {
                $sql1 = "UPDATE accounts SET sick_Leave = sick_Leave - $duration WHERE user = '$user'";
            }

            if($type == "Vacation" || $type == "vacation" )
            {
                $sql1 = "UPDATE accounts SET vac_Leave = vac_Leave - $duration WHERE user = '$user'";
            }

            #Applies the SQL statement for deduction
            $result1 = mysqli_query($conn, $sql1);
        }

        else if($condition == "1" || $condition > "2")
        {
            echo "There was no change";
        }

        #echo $sql1. "<br>". $sql;

        header("location: leaveManager.php");
    } 
?>