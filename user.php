<?php 
 
session_start();
 //echo !isset($_SESSION['logged_in']) ;die();
 if (!isset($_SESSION['logged_in'])) {//============================
  header("location: login.php");    //==============================
}

else{ 
 
 $errors = array();
 include('connect.php');

$uname=$_SESSION['user'];

 if(isset($_POST['employee'])){
    $uname = $_POST['employee'];
    $sql = "SELECT * FROM accounts WHERE user = '$uname' ";
    $sqlresult = mysqli_query($conn,$sql);

    while($row = mysqli_fetch_array($sqlresult)) 
        {
                $idnumber = $row['idnumber'];
                $firstname = $row['first_name'];
                $lastname= $row['Last_name'];
                $position_type = $row['position_type'];
                $user =$row['user'];
        }

    }

   if (isset($_POST['update']))
    {
        $firstname=$_POST['firstname'];
        $lastname=$_POST['lastname'];
        $position=$_POST['type'];
        $password_1=$_POST['password_1'];
        $password_2=$_POST['password_2'];

        if (empty($firstname))
        {
            array_push($errors, "firstname is required");
        }
        if (empty($lastname))
        {
            array_push($errors, "lastname is required");
        }
        if (empty($position))
        {
            array_push($errors, "position is required");
        }
        if ($password_1 != $password_2)
        {
            array_push($errors, "the two password do not match");
        }


        if (count($errors)==0)
        {
            $password = md5($password_1);
            $fullname = $firstname." ".$lastname; 

            if(empty($password_1)){
            $sql="UPDATE accounts 
            SET first_name = '$firstname', Last_name = '$lastname', position_type = '$position' WHERE user = '$uname'";
            }
            else{
            $sql="UPDATE accounts 
            SET first_name = '$firstname', Last_name = '$lastname', position_type = '$position', password = '$password' WHERE user = '$uname'";
            }

            mysqli_query($conn,$sql);
            echo "<div class='uk-alert-success' uk-alert><a class='uk-alert-close' uk-close></a>Account Updated</div>";

        }
    }
$sql = "SELECT * FROM accounts WHERE user = '$uname' ";
$sqlresult = mysqli_query($conn,$sql);
$row = mysqli_fetch_array($sqlresult);
$firstname = $row['first_name'];
$lastname= $row['Last_name'];
$position_type = $row['position_type'];

$sqll = "SELECT * FROM accounts ORDER BY first_name";
$sqllresult = mysqli_query($conn,$sqll);
}
?>

<!DOCTYPE html>
<html>
<head>


 <title> TAS TRADESOFT - Account Management</title>



 <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/uikit.css" />
        <script src="js/uikit.min.js"></script>
        <script src="js/uikit-icons.min.js"></script>
 
</head>
<body>
    <?php   include ('errors.php');?>

    <nav class="uk-navbar-container uk-margin" uk-navbar>
    <div class="uk-navbar-left">

        <ul class="uk-navbar-nav">
            <li>
                <a href="admin.php">
                    TAS Tradesoft Expense Report
                </a>
            </li>
        </ul>

        
    </div>

    
</nav>
    
  
    <div class="uk-inline uk-position-center">


    
       <div class="uk-card uk-card-medium uk-card-default uk-position-center uk-width-1-2@m">
            
            <div class="uk-container uk-container-large ">

                
            
                <div class="uk-card-header">
                    <h3 class="uk-card-title uk-margin-remove-bottom">Account Management</h3>
                </div>
              
               <form method= "post">
                    <div class="uk-card-body">

                   
                        <label class="uk-form-label" for="form-horizontal-text">Employee</label>
                            <div class="form-input ">
                        <select class="uk-select" id="form-horizontal-employee" name="employee" onchange="this.form.submit();">
                         <?php 
                         while($row = mysqli_fetch_array($sqllresult)){
                             $sel = ''; 
                             if($row['user'] == $uname){ 
                              $sel = 'selected'; 
                            }
                             echo "<option $sel value='" . $row['user'] ."'>" . $row['first_name']." ".$row['Last_name'] ."</option>";
                            }
                        ?>
                         </select>
                            </div>
                        <label class="uk-form-label" for="form-horizontal-text">First Name</label>
                            <div class="form-input ">
                                <input class="uk-input uk-form-width-medium" type="text" name="firstname" value="<?php echo $firstname; ?>" /> 
                            </div>
                        <label class="uk-form-label" for="form-horizontal-text">Last Name</label>
                            <div class="form-input ">
                                <input class="uk-input uk-form-width-medium" type="text" name="lastname" value="<?php echo $lastname; ?>"/> 
                            </div>                        
                        <label class="uk-form-label" for="form-horizontal-text">Position</label>
                         <div  class="uk-margin">
                                    <select name = "type" class="uk-select" class="uk-margin-small-bottom uk-margin-small-top uk-select uk-form-small">
                                        <?php  if($position_type == 'admin'){
                                           echo "<option selected>admin</option>
                                                 <option>employee</option>";
                                        }
                                        else{
                                            echo "<option>admin</option>
                                                 <option selected>employee</option>";                                           
                                        }
                                            ?>
                                    </select>
                        </div>
                        <label class="uk-form-label" for="form-horizontal-text">New Password</label>
                            <div class="form-input">
                                <input class="uk-input uk-form-width-medium"  type="password" name="password_1" />
                            </div> 
                        <label class="uk-form-label" for="form-horizontal-text">Confirm New Password</label>
                            <div class="form-input">
                                <input class="uk-input uk-form-width-medium"  type="password" name="password_2" />
                            </div>
                    </div>
                    
                    <div class="uk-card-footer">
                        <button type= "submit" name="update" class="uk-button uk-button-primary">UPDATE</button>
                    </div>

            </div>
            </form>
           </div>
        </div>
        </div>
    </div>

    
    
</body>
</html>