-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.32-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for tas
DROP DATABASE IF EXISTS `tas_receipt`;
CREATE DATABASE IF NOT EXISTS `tas_receipt` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `tas_receipt`;

-- Dumping structure for table tas.accounts
DROP TABLE IF EXISTS `accounts`;
CREATE TABLE IF NOT EXISTS `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idnumber` int(11) NOT NULL DEFAULT '0',
  `first_name` varchar(50) NOT NULL,
  `Last_name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `position_type` text NOT NULL,
  `user` varchar(50) NOT NULL,
  `requiredamount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table tas.receipt
DROP TABLE IF EXISTS `receipt`;
CREATE TABLE IF NOT EXISTS `receipt` (
  `sn` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `storename` varchar(30) NOT NULL,
  `tin` varchar(30) NOT NULL,
  `address` varchar(40) NOT NULL,
  `type` text NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `vat` double NOT NULL,
  `vatAmount` decimal(10,2) NOT NULL,
  `nonVat` decimal(10,2) NOT NULL,
  `employee` varchar(50) NOT NULL,
  `vatableAmount` decimal(10,2) NOT NULL,
  PRIMARY KEY (`sn`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
