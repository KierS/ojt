<?php
	session_start();
	include ('connect.php');

	if ($conn->connect_error) 
	{
    	die("Connection failed: " . $conn->connect_error);
	}

	if(isset($_POST['search']))
	{
		if ($position=="Admin")
		{
			$from_date = $_POST['from_date'];
			$to_date = $_POST['to_date'];
			$typefilter = $_POST['typefilter'];
			$namefilter = $_POST['nameToSearch'];
			$namesession = $_SESSION['user'];
			$query = "SELECT * FROM receipt";
			$conditions = array();

			if(!empty($from_date) AND !empty($to_date))
			{
				$conditions[] = "(date BETWEEN '$from_date' AND '$to_date')";
			}
			else
			{
				$conditions[] = "MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE())";
			}

			if(!empty($typefilter))
			{
				$conditions[] = "type='$typefilter'";
			}
		
			if(!empty($namefilter))
			{
				if($_POST['nameToSearch'] == 'all')
				{

				}
				else
				{
					$conditions[] = "employee= '$namefilter'";
				}
			}

			else
			{
				$conditions[] = "employee = '$namesession'";
			}
			
			$sql = $query;
			
			if (count($conditions)>0)
			{
				$sql .= " WHERE " . implode(' AND ',$conditions);
			}

        	$sql .= " ORDER BY date ASC";
		}

		else
		{
			$from_date = $_POST['from_date'];
			$to_date = $_POST['to_date'];
			$typefilter = $_POST['typefilter'];
			$namesession = $_SESSION['user'];
			$query = "SELECT * FROM receipt";
			$conditions = array();

			if(!empty($from_date) AND !empty($to_date))
			{
				$conditions[] = "(date BETWEEN '$from_date' AND '$to_date')";
			}

			else
			{
				$conditions[] = "MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE())";
			}

			if(!empty($typefilter))
			{
				$conditions[] = "type='$typefilter'";
			}

			$conditions[] = "employee = '$namesession'";
			$sql = $query;

			if (count($conditions) > 0)
			{
				$sql .= " WHERE " . implode(' AND ',$conditions);
				$_SESSION['SQL'] = $sql;
			}

		}
	}
	else
	{
		if ($position=="Admin")
		{
			$namefilter = $_SESSION['user'];

			#DEFAULT SQL STATEMENT
			$sql ="SELECT * FROM receipt ORDER BY DATE(date) ASC";

			$sql1="SELECT sum(amount) AS total FROM receipt  ";
			$sql2="SELECT sum(vatAmount) AS total1 FROM receipt  ";
			$sql3="SELECT sum(expense) AS total2 FROM receipt  ";
			$sql4="SELECT requiredamount  AS Rbalance FROM accounts WHERE user='' ";
			$_SESSION['SQL'] = $sql;
		}
		else
		{
			$namefilter = $_SESSION['user'];
			$sql="SELECT * FROM receipt WHERE
			employee= '$namefilter' AND MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE()) ORDER BY date ASC";
			$_SESSION['SQL'] = $sql;
		}
	}
	

	if(isset($_POST['searchemployee']))
	{	
		if($position == "Admin")
		{
			$sdate = $_POST['from_date'];
			$fdate = $_POST['to_date'];
			$type = $_POST['typefilter'];
			$employee = $_POST['employee'];
			$conditions = array();

			$sqlsrch = "SELECT * FROM receipt";
		
			if(!empty($sdate) AND !empty($fdate))
			{
				$conditions[] = "(date BETWEEN '$sdate' AND '$fdate')";
			}
			if(!empty($employee))
			{
				$conditions[] = "employee = '$employee'";
			}
			if(!empty($type))
			{
				$conditions[] = "type = '$type'";
			}

			if (count($conditions)>0)
			{
				$sqlsrch .= " WHERE " . implode(' AND ',$conditions);
			}
			#echo $sqlsrch;
		
			#checks valid SQL Statement
			$resultsrch = mysqli_query($conn,$sqlsrch);
			#Loops while all the affected arrays are processed individually
			$rowz = mysqli_fetch_array($resultsrch);
			$sql = $sqlsrch;
		}
	}

	

	#Transfer of SQL statement to next window (Table content transferring)
	$_SESSION['query'] = $sql;


	if ($result=mysqli_query($conn,$sql))
	{
		echo "  <table style='width:100%'>";
		?>
		<form method="POST" action="delete.php" onSubmit="return confirm('Delete Selected Items?');">
		<?php
		echo "	<tr>
						<th style='color:DodgerBlue; width:2% ;border-bottom: 2px solid #0dd;padding:5px'>
							<input type='submit' name='deleteall' value='Delete' class='uk-button uk-button-primary uk-button-small'>
						</th>
						<th style='color:DodgerBlue; width:5% ;border-bottom: 2px solid #0dd;padding:5px'><a?deleteAll&employee=".$namefilter."' class='uk-table-shrink' style= 'color:DodgerBlue '>Delete<onClick=\"return confirm('are you sure you want to delete All?');\"</a></th>
						<th style='color:DodgerBlue; width:5% ;border-bottom: 2px solid #0dd;padding:5px'>Edit</th>
						<th style='color:DodgerBlue; width:5% ;border-bottom: 2px solid #0dd;padding:5px'>Tin</th>
						<th style='color:DodgerBlue; width:12%;border-bottom: 2px solid #0dd;padding:5px'>Date</th>
						<th style='color:DodgerBlue; width:8% ;border-bottom: 2px solid #0dd;padding:5px'>Store Name</th>
						<th style='color:DodgerBlue; width:10%;border-bottom: 2px solid #0dd;padding:5px'>Type</th>
						<th style='color:DodgerBlue; width:9% ;border-bottom: 2px solid #0dd;padding:5px'>Gross Expense</th> 	
						<th style='color:DodgerBlue; width:7% ;border-bottom: 2px solid #0dd;padding:5px'>VAT Rate</th>
						<th style='color:DodgerBlue; width:9% ;border-bottom: 2px solid #0dd;padding:5px'>Non-Vatables</th>
						<th style='color:DodgerBlue; width:10%;border-bottom: 2px solid #0dd;padding:5px'>Net Amount</th>
						<th style='color:DodgerBlue; width:10%;border-bottom: 2px solid #0dd;padding:5px'>Vatable Amount</th>
						<th style='color:DodgerBlue; width:15%;border-bottom: 2px solid #0dd;padding:5px'>Employee</th>
					</tr>";
		while($row = mysqli_fetch_array($result))
		{
			if($_SESSION['position'] == "Admin")
				$link = "admin.php";
			else
				$link = "sample.php";
			$sn = $row['sn'];
			$source = $row['date'];
			$date =new dateTime($source);
			$storename = $row['storename'];
			$type = $row['type'];
			$vatRate =$row['vat'];
			$nonVat = $row['nonVat'];
			$amount = $row['amount'];
			$vatAmount = $row['vatAmount'];
			$vatableAmount = $row['vatableAmount'];
			$emp = $row['employee'];
			$tin = $row['tin'];
			$address = $row['address'];
			echo "	<tr>
						<td style='text-align:center;border-bottom: 1px solid #add;padding:8px'>
							<input type='checkbox' name='checkbox[]' value='".$row['sn']."'>
						</td>
						<td style='text-align:center;border-bottom: 1px solid #ddd;padding:8px'>
						<a href='delete.php?sn=".$sn."'; onClick=\"return confirm('Are you sure you want to delete?');\" uk-icon='trash'; uk-toggle></a>
						</td>
						<td style='text-align:center;border-bottom: 1px solid #ddd;padding:8px'>
						<a href='".$link."?sn=".$sn."'; onClick='changeHeader()' ; uk-icon='file-edit'; uk-toggle></a></td>
						<td style='text-align:center;border-bottom: 1px solid #ddd;padding:8px'>".$tin."</td>
						<td style='text-align:center;border-bottom: 1px solid #ddd;padding:8px'>".$date->format('M-d-Y')."</td>
						<td style='text-align:center;border-bottom: 1px solid #ddd;padding:8px'>".$storename."</td>
						<td style='text-align:center;border-bottom: 1px solid #ddd;padding:8px'>".$type."</td>
						<td style='text-align:center;border-bottom: 1px solid #ddd;padding:8px'> ₱ ".number_format($amount,2)."</td>
						<td style='text-align:center;border-bottom: 1px solid #ddd;padding:8px'> ".($vatRate*100)."%</td>
						<td style='text-align:center;border-bottom: 1px solid #ddd;padding:8px'> ₱ ".number_format($nonVat,2)."</td>
						<td style='text-align:center;border-bottom: 1px solid #ddd;padding:8px'> ₱ ".number_format($vatAmount,2)."</td>
						<td style='text-align:center;border-bottom: 1px solid #ddd;padding:8px'> ₱ ".number_format($vatableAmount,2)."</td>
						<td style='text-align:center;border-bottom: 1px solid #ddd;padding:8px'>".$emp."</td>
				  	</tr>";
		}
 		echo "</table>";

		mysqli_free_result($result);
	}
	mysqli_close($conn);

?>
