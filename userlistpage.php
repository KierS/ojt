<?php
   session_start();
   error_reporting(E_PARSE | E_ERROR);
   include("connect.php");

   if (!isset($_SESSION['logged_in'])) 
   {
    header("location: login.php"); 
   }

if(isset($_POST['editThis']))
{
    $idnumber = $_POST['idnumber'];
    $id = $_POST['idofficial'];
    $fname = $_POST['first_name'];
    $lname = $_POST['last_name'];
    $birthdate = $_POST['birthdate'];
    $home = $_POST['home_Address'];
    $sick = $_POST['sick_Leave'];
    $postype = $_POST['position'];
    $vac = $_POST['vac_Leave'];
    $phic = $_POST['phic'];
    $sss = $_POST['sss'];
    $pagibig = $_POST['pagibig'];
    $tin = $_POST['tin'];
    $department = $_POST['department'];
    $datehired = $_POST['datehired'];
    $requiredamount = $_POST['requiredamount'];
    
    $setThese = array();

    $sqlnew = "UPDATE accounts SET ";

    #CHECK FOR VALUES TO BE UPDATED
    if(!empty($_POST['idnumber']))
    {
        $setThese[] = " idnumber = "."'". $idnumber."'";
    }
    if(!empty($_POST['first_name']))
    {
        $setThese[] = " first_name = "."'". $fname."'";
    }
    if(!empty($_POST['last_name']))
    {
        $setThese[] = " last_name = ". "'". $lname."'";
    }
    if(!empty($_POST['position']))
    {
        $setThese[] = " position_type = ". "'". $postype."'";
    }
    if(!empty($_POST['birthdate']))
    {
        $setThese[] = " birthdate = ". "'". $birthdate."'";
    }
    if(!empty($_POST['phic']))
    {
        $setThese[] = " phic = ". "'". $phic."'";
    }
    if(!empty($_POST['home_address']))
    {
        $setThese[] = " home_Address = ". "'". $haddress."'";
    }
    if(!empty($_POST['pagibig']))
    {
        $setThese[] = " pagibig = ". "'". $pagibig."'";
    }
    if(!empty($_POST['sss']))
    {
        $setThese[] = " sss = ". "'". $sss."'";
    }
    if(!empty($_POST['tin']))
    {
        $setThese[] = " tin = ". "'". $tin."'";
    }
    if(!empty($_POST['department']))
    {
        $setThese[] = " department = ". "'". $department."'";
    }
    if(!empty($_POST['quota']))
    {
        $setThese[] = " requiredamount = ". "'". $quota."'";
    }
    #echo $birthdate;    

    $sqlnew .= implode(', ', $setThese). " WHERE id = ".$id;
    #echo $sqlnew;

    mysqli_query($conn, $sqlnew);
    #echo mysqli_error($conn);
}

include ('connect.php');

$userEmployee=$_SESSION['user'];
$position=$_SESSION['position'];
$userID=$_SESSION['idnumber'];

$sql = "SELECT * FROM accounts WHERE idnumber = ".$_GET['id']. " ORDER BY id";
$sqlresult = mysqli_query($conn,$sql);
$row = mysqli_fetch_array($sqlresult);

$requiredamount = $row['requiredamount'];
$_SESSION["SQL"] = $sql;

if($position!="Admin"||$position!="admin"){
    header("location: sample.php");
}

if(isset($_POST['submit']))
        { 
            $idnumber = $_POST['idnumber'];
            $lname = $_POST['last_name'];
            $fname = $_POST['first_name'];
            $postype = $_POST['postype'];
            $date = $_POST['date'];
            $hdate = $_POST['hdate'];
            $haddress = $_POST['home_address'];
            $phic = $_POST['phic'];
            $sss = $_POST['sss'];
            $tin = $_POST['tin'];
            $department = $_POST['department'];
            $pagibig = $_POST['pagibig'];
            $quota = $_POST['quota'];
            
            $user = $fname." ".$lname;
            $password = md5($_POST['password']);

            $NonVatAmount = $_POST['NonVatAmount'];
            $VATAMOUNT = ($AMOUNT - $NonVatAmount)/(1+$VAT);//VATAMOUNT CHANGE TO NET AMOUNT
            $vatableAmount = ($VATAMOUNT*$VAT);//new VATTABLE amount

                $sql = "INSERT INTO accounts (idnumber, last_name, first_name, position_type, birthdate, home_Address, phic, sss, tin, department, pagibig, requiredamount, user, password, datehired)
                        VALUES ('$idnumber', '$lname', '$fname', '$postype', '$date', '$haddress', '$phic','$sss','$tin','$department' , '$pagibig', '$quota', '$user', '$password', '$hdate')";

            if ($conn->query($sql) === TRUE) 
                {
                    echo "<div class='uk-alert-success' uk-alert><a class='uk-alert-close' uk-close></a>User recorded</div>";
                } 
            else 
            {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
        }


$TODAY=date('m/d/Y');
?>

<html>
<head>
   
    <title> TAS Tradesoft - User List </title>
    <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/uikit.css" />
        <script src="js/uikit.min.js"></script>
        <script src="js/uikit-icons.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>

<script>
    $(document).ready(function() 
    {
        $('#modal-container').modal(show);
    });
</script>
<body>
    <!-- Navigation Bar -->
    <?php
        include("navbar.php");
    ?>
       
    <!-- Navigation Bar End -->

    <div class = "uk-container">

            <div style= "width:100%;">
                <form action="userlistpage.php" method ="post" style = "margin-bottom: 0px;">
                    <div class="uk-inline">
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-date">Search:</label>
                            <div class="uk-form-controls">
                                <input class="uk-input uk-form-small"  id="search" name="searchthis" type="input">
                            </div>
                        </div>
                    </div>
                    <div class="uk-inline">
                        <div uk-form-custom="target: > * > span:first-child">
                            <select name= "typefilter">
                                <option value="">Category</option>
                                <option value="idnumber">ID Number</option>
                                <option value="last_name">Last Name</option>
                                <option value="first_name">First Name</option>
                                <option value="position_type">Position</option>
                                <option value="departent">Department</option>
                                <option value="phic">Philippine Health</option>             
                                <option value="pagibig">Pag-Ibig</option>   
                                <option value="sss">SSS</option>   
                                <option value="tin">Tin</option> 
                                <option value="requiredamount">Required Amount</option>     
                            </select>
                            <button class="uk-button uk-button-default uk-form-small" style = "width=200px" type="button" tabindex="-1">
                                <span></span>
                                <span uk-icon="icon: triangle-down"></span>
                            </button>
                        </div>
                    </div>

                    <div class="uk-inline">
                        <input type = "submit" value="Search" class="uk-input uk-button-primary uk-form-small" name="search"><br/>
                    </div>
                </form>
                   
                    <p class = "uk-textarea uk-padding-remove uk-width-9-10"; style="width:100%; height:auto"; >
                        <?php 
                            include "userlist.php"; 
                        ?>
                    </p>
                </div>
                <!-- End of 2nd Column -->
        </div>
    </div>
</body>
</html>