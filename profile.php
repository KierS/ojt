<?php

     session_start();
     error_reporting(E_PARSE | E_ERROR);
    
     if (!isset($_SESSION['logged_in'])) {
      header("location: login.php");    
    }

    include ('connect.php');

    $userEmployee=$_SESSION['user'];
    $position=$_SESSION['position'];
    $userID=$_SESSION['idnumber'];
    $bdate=$_SESSION['birthdate'];

    $sql = "SELECT * FROM accounts WHERE user = '$userEmployee'";
    $sqlresult = mysqli_query($conn,$sql);
    $row = mysqli_fetch_assoc($sqlresult);
?>

<html>
    <head>
        <title> TAS Tradesoft - Employee Profile </title>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/uikit.css" />
        <script src="js/uikit.min.js"></script>
        <script src="js/uikit-icons.min.js"></script>
        <script type="text/javascript" src="js/timeScripts.js"></script>
    </head>
        <body>
        
            <div class = "profilebar">

                <nav class="uk-navbar-container" uk-navbar>
                    
                    <div class="uk-navbar-left">

                        <ul class="uk-navbar-nav">
                            <li>
                                <a href="sample.php" class="uk-navbar uk-logo">
                                TAS Tradesoft CORP.
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="uk-navbar-right">
                        
                        <ul class="uk-navbar-nav"    action ="logout">
                            <li> 
                                <a href="logout.php">
                                    Log-Out
                                </a>
                            </li>
                        </ul>
                        
                    </div>

                </nav>

            </div>

            <div class="uk-section uk-section-large uk-position-top-left">
                
                <div class="uk-container">

                    <h1 class="uk-text-uppercase">
                        <?php echo $userEmployee ?>
                    </h1>

                 </div>

            </div>
            
            <div class="uk-section uk-section-large uk-position-center-left"> 
                
                <div class="uk-grid-match uk-child-width-auto" uk-grid>
                        
                        <div>
                            <h3 class="uk-text-lead "> Department: <?php echo $row['department'] ?> </h3>
                            <h3 class="uk-text-lead"> Position: <?php echo $row['position'] ?> </h3>
                            <h3 class="uk-text-lead"> Date Hired: <?php echo $row['datehired'] ?> </h3>

                        </div>

                    </div>  
            </div>

                <div class="uk-section uk-section-large uk-position-center-right"> 
                
                    <div class="uk-grid-match uk-child-width-auto" uk-grid>
                        
                        <div>
                            <h3 class="uk-text-lead"> Birthday: <?php echo date('m-d-Y',strtotime($row['birthdate`'])); ?> </h3>
                            <h3 class="uk-text-lead"> Address: <?php echo $row['address'] ?> </h3>
                            <h3 class="uk-text-lead"> PHIC: <?php echo $row['phic'] ?> </h3>
                            <h3 class="uk-text-lead"> Pag Ibig: <?php echo $row['pagibig'] ?> </h3>
                            <h3 class="uk-text-lead"> SSS: <?php echo $row['sss'] ?> </h3>
                            <h3 class="uk-text-lead"> TIN: <?php echo $row['tin'] ?> </h3>

                        </div>

                </div>

                <div class="uk-section uk-section-large uk-position-right"> 
                
                    <div class="uk-grid-match uk-child-width-auto" uk-grid>
                    
                        <div>
                            <h3 class="uk-text-lead"> Birthday: <?php echo date('m-d-Y',strtotime($row['birthdate`'])); ?> </h3>
                            <h3 class="uk-text-lead"> Address: <?php echo $row['address'] ?> </h3>
                            <h3 class="uk-text-lead"> PHIC: <?php echo $row['phic'] ?> </h3>
                            <h3 class="uk-text-lead"> Pag Ibig: <?php echo $row['pagibig'] ?> </h3>
                            <h3 class="uk-text-lead"> SSS: <?php echo $row['sss'] ?> </h3>
                            <h3 class="uk-text-lead"> TIN: <?php echo $row['tin'] ?> </h3>

                        </div>

                </div>
            </div>

        </body>
</html>