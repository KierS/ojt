<?php 
 
session_start();
 //echo !isset($_SESSION['logged_in']) ;die();
 if (!isset($_SESSION['logged_in'])) {//============================
  header("location: login.php");    //==============================
}

else{ 

 include('connect.php');

 include ('server.php');
}

$sql="SELECT * FROM accounts WHERE idnumber = ". $_GET['idnumber'];

$result=mysqli_query($conn,$sql);

$row=mysqli_fetch_array($result);


?>


<!DOCTYPE html>
<html>
<head>


 <title> TAS TRADESOFT - Expense Report</title>



 <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/uikit.css" />
        <script src="js/uikit.min.js"></script>
        <script src="js/uikit-icons.min.js"></script>
 
</head>
<body>
    <?php   include ('errors.php');?>

    <nav class="uk-navbar-container uk-margin" uk-navbar>
        <div class="uk-navbar-left">
            <ul class="uk-navbar-nav">
                <li>
                    <a href="admin.php">
                        TAS Tradesoft Expense Report
                    </a>
                </li>
            </ul>        
        </div>    
    </nav>
    <!-- REGISTRATION FORM -->
    
    <!-- REGISTRATION BUTTON -->
<a class="uk-button uk-button-default" style="max-width:100px"; href="#modal-group-1" uk-toggle>Open</a>

    <!-- REGISTRATION FORM -->

   <div id="modal-group-1" uk-modal>
        <div class="uk-modal-dialog">
            <div class="uk-modal-header">
                <h2 class="uk-modal-title">Edit Profile: <?php echo $row['last_name'].", ". $row['first_name'] ?></h2>
            </div>
            <div class="uk-modal-body">
                <form method= "post" action="editprofile.php">
                    <div class="uk-card-body">
                        <div class="form-input">
                            <label class="uk-form-label" for="form-horizontal-text">ID Number</label><br style="max-height: 2px">   
                            <input class="uk-input uk-form-width-medium" type="text" name="idnumber" placeholder= <?php echo $row['idnumber'] ?>> 
                        </div>
                        <div class="form-input"; style="float:left; margin-right:4px;">
                            <label class="uk-form-label" for="form-horizontal-text">Last Name</label><br style="max-height:2px">
                            <input class="uk-input uk-form-width-medium" type="text" name="lastname" placeholder=<?php echo $row['last_name'] ?>> 
                        </div>
                        <div class="form-input ">
                            <label class="uk-form-label" for="form-horizontal-text">First Name</label><br style="max-height:2px">
                            <input class="uk-input uk-form-width-medium" type="text" name="firstname" placeholder=<?php echo $row['first_name'] ?>> 
                        </div>
                        <div class="form-input ">
                            <label class="uk-form-label" for="form-horizontal-text">Birth Date</label><br style="max-height:2px">
                            <input class="uk-input uk-form-width-medium" type="date" name="birthdate" placeholder=<?php echo $row['birthdate'] ?>> 
                        </div>
                        <div class="form-input uk-child-width-9-10">
                            <label class="uk-form-label" for="form-horizontal-text">Home Address</label><br style="max-height:2px">
                            <input class="uk-input uk-form-width-medium" type="text" name="homeaddress" placeholder=<?php echo $row['home_Address'] ?>> 
                        </div>
                        <div class="form-input uk-child-width-1-4">
                            <label class="uk-form-label" for="form-horizontal-text">Philippine Health</label><br style="max-height:2px">
                            <input class="uk-input uk-form-width-medium" type="text" name="philhealth" placeholder=<?php echo $row['phic'] ?>> 
                        </div>
                        <div class="form-input uk-child-width-1-4">
                            <label class="uk-form-label" for="form-horizontal-text">Pag-Ibig</label><br style="max-height:2px">
                            <input class="uk-input uk-form-width-medium" type="text" name="pagibig" placeholder=<?php echo $row['pagibig'] ?> > 
                        </div>
                        <div class="form-input uk-child-width-1-4">
                            <label class="uk-form-label" for="form-horizontal-text">SSS</label><br style="max-height:2px">
                            <input class="uk-input uk-form-width-medium" type="text" name="sss" placeholder=<?php echo $row['sss'] ?>> 
                        </div>
                        <div class="form-input uk-child-width-1-4">
                            <label class="uk-form-label" for="form-horizontal-text">Tin</label><br style="max-height:2px">
                            <input class="uk-input uk-form-width-medium" type="text" name="tin" placeholder=<?php echo $row['tin'] ?>> 
                        </div>
                        <div class="form-input ">
                            <label class="uk-form-label" for="form-horizontal-text">Department</label><br style="max-height:2px">
                            <input class="uk-input uk-form-width-medium" type="text" name="department" placeholder=<?php echo $row['department'] ?>> 
                        </div>
                        <div class="form-input ">
                            <label class="uk-form-label" for="form-horizontal-text">Position</label><br style="max-height:2px">
                            <select name = "type" class="uk-select" class="uk-margin-small-bottom uk-margin-small-top uk-select uk-form-small">
                                <option value= "" disabled selected>Previous Position: <?php echo $row['position_type'] ?></option>
                                <option>Admin</option>
                                <option>Employee</option>
                            </select>
                        </div>
                        <div class="form-input ">
                            <label class="uk-form-label" for="form-horizontal-text">Date Hired</label><br style="max-height:2px">
                            <input class="uk-input uk-form-width-medium" type="date" name="datehired" /> 
                        </div>
                        <div class="form-input ">
                            <label class="uk-form-label" for="form-horizontal-text">Quota</label><br style="max-height:2px">
                            <input class="uk-input uk-form-width-medium" type="text" name="requiredamount" placeholder=<?php echo $row['requiredamount'] ?>> 
                        </div>                            
                        <div class="form-input ">
                            <label class="uk-form-label" for="form-horizontal-text">Password</label><br style="max-height:2px">
                            <input class="uk-input uk-form-width-medium"  type="password" name="password_1" />
                        </div>
                        <div class="form-input ">
                            <label class="uk-form-label" for="form-horizontal-text">Confirm Password</label><br style=" :2px">
                            <input class="uk-input uk-form-width-medium"  type="password" name="password_2" />
                        </div>
                </div>     
                <div class="uk-card-footer">
                    <button type= "submit" name="edit" class="uk-button uk-button-primary ">Edit</button>
                </div>
            </form>
    </div> 
</div>
</div>    
</body>
</html>