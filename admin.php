

<?php

 session_start();
 error_reporting(E_PARSE | E_ERROR);
 //echo isset($_SESSION['logged_in_admin']) ;die();
    if (!isset($_SESSION['logged_in'])) 
    {
        header("location: login.php");    
    }
    else
    {
        
        include('connect.php');
//================================
        $username=$_SESSION['user'];
        $position=$_SESSION['position'];
        $userID=$_SESSION['idnumber'];
//=================================
        $sqll = "SELECT * FROM accounts WHERE idnumber > 0 ORDER BY idnumber ";
        #echo $sqll;

        $TODAY= date('m-d-Y');

        if($position=="Employee")
        {
            header("location: sample.php");
        }
        

        if(isset($_POST['submit']))
        { 
            $DATE = $_POST['date'];
            $STORENAME = $_POST['storename'];
            $TIN = $_POST['tin'];
            $TYPE = $_POST['type'];
            $ADDRESS = $_POST['address'];
            $EMPLOYEE= $_POST['employee'];

            $AMOUNT = $_POST['amount'];
            $VAT = $_POST['vat'];
            $NonVatAmount = $_POST['NonVatAmount'];
            $VATAMOUNT = ($AMOUNT - $NonVatAmount)/(1+$VAT);//VATAMOUNT CHANGE TO NET AMOUNT
            $vatableAmount = ($VATAMOUNT*$VAT);//new VATTABLE amount

            if (empty($DATE))
            {
                $sql = "INSERT INTO receipt (date, storename, type, amount, vat, nonVat, vatAmount, employee,vatableAmount,address,tin)
                        VALUES (NOW(), '$STORENAME', '$TYPE', '$AMOUNT', '$VAT', '$NonVatAmount', '$VATAMOUNT', '$EMPLOYEE','$vatableAmount','$ADDRESS','$TIN')";
            }
            else
            {
                $sql = "INSERT INTO receipt (date, storename, type, amount, vat, nonVat, vatAmount, employee,vatableAmount,address,tin)
                VALUES ('$DATE', '$STORENAME', '$TYPE', '$AMOUNT', '$VAT', '$NonVatAmount', '$VATAMOUNT', '$EMPLOYEE','$vatableAmount','$ADDRESS','$TIN')";
            }

            if( empty($STORENAME) || empty($AMOUNT) || empty($VAT) || empty($VATAMOUNT))
                {
                    echo "<div class='uk-alert-danger' uk-alert><a class='uk-alert-close' uk-close></a>You did not fill out the required fields.</div>";
                }
            else if ($conn->query($sql) === TRUE) 
                {
                    if($AMOUNT>=$requiredamount)
                    {
                        echo "<div class='uk-alert-success' uk-alert><a class='uk-alert-close' uk-close></a>Expense recorded, You reached your quota</div>";
                    }
                    else
                    {
                        echo "<div class='uk-alert-success' uk-alert><a class='uk-alert-close' uk-close></a>Expense recorded</div>";
                    }

                } 
            else 
            {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
        }



        if(!empty($_POST['nameToSearch']))
        {
            $sql2 = "SELECT * FROM accounts WHERE user = '".$_POST['nameToSearch']."'";
        }
        else
        {
            $sql2 = "SELECT * FROM accounts WHERE user = '$username'";
        }

        $sql2result = mysqli_query($conn,$sql2);
        $row = mysqli_fetch_array($sql2result);
        $requiredamount = $row['requiredamount'];

        $from_date = $_POST['from_date'];
        $to_date = $_POST['to_date'];
        $typefilter = $_POST['typefilter'];
        $namefilter = $_POST['nameToSearch'];

        $q  = "SELECT sum(amount) AS total FROM receipt";
        $q2 = "SELECT sum(vatAmount) as total1 FROM receipt"; 
        $q3 = "SELECT sum(vatableAmount) as total2 FROM receipt";  
        $q4 = "SELECT sum(nonVat) as total3 FROM receipt"; 
    
        $conditions = array();        

        if(!empty($from_date) AND !empty($to_date))
        {
            $conditions[] = "(date BETWEEN '$from_date' AND '$to_date')";
        }
        else
        {
            $conditions[] = "MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE())";
        }

        if(!empty($typefilter))
        {
            $conditions[] = "type='$typefilter'";
        }

        else
        {
            $conditions[] = "employee = '$username'";
            $_SESSION['employ'] = $username;
        }
        
        $sqlgross = $q;
        $sqlvat = $q2;
        $sqlvatable = $q3;
        $sqlnonvat = $q4;

        if (count($conditions)>0)
        {
            $sqlgross   .= "WHERE " . implode(' AND ',$conditions);
            $sqlvat     .= "WHERE " . implode(' AND ',$conditions);
            $sqlvatable .= "WHERE " . implode(' AND ',$conditions);
            $sqlnonvat  .= "WHERE " . implode(' AND ',$conditions);
        }

        $_SESSION['sqlgross']       = $sqlgross;
        $_SESSION['sqlvat']         = $sqlvat;
        $_SESSION['sqlvatable']     = $sqlvatable;
        $_SESSION['sqlnonvat']      = $sqlnonvat;

        

        $result1 = mysqli_query($conn,$sqlgross);
        $row1 = mysqli_fetch_array($result1);
        $amount1 = $row1['total'];


        $result2=mysqli_query($conn,$sqlvat);
        $row2 = mysqli_fetch_array($result2);
        $amount2 = $row2['total1'];

        $result3=mysqli_query($conn,$sqlvatable);
        $row3 = mysqli_fetch_array($result3);
        $amount3 = $row3['total2'];

        $result4=mysqli_query($conn,$sqlnonvat);
        $row4 = mysqli_fetch_array($result4);
        $amount4 = $row4['total3'];
            
            $sqlUpdate = "SELECT * FROM receipt WHERE sn = ". $_GET['sn'];
            $resultUpdate=mysqli_query($conn,$sqlUpdate);
            $rowUpdate=mysqli_fetch_array($resultUpdate);
            $sqlEdit .= "5";
            $resultEdit = mysqli_query($conn,$sqlEdit);


        $remainbalan = ($requiredamount - $amount1);

        #RECEIPT CALCULATOR
        $sqlReceipt = "SELECT * FROM receipt";
        $sqlReceiptResult = mysqli_query($conn, $sqlReceipt);
        
        #INITIALIZATIONS
        $totalAmount;
        $totalVatAmount;
        $totalNonVat;
        $totalVatableAmount;

        #TOTAL AMOUNTS CALCULATOR 
        while($row = mysqli_fetch_array($sqlReceiptResult))
        {
            $amount = $row['amount'];
            $vatAmount = $row['vatAmount'];
            $nonVat = $row['nonVat'];
            $vatableAmount = $row['vatableAmount'];
            
            $totalAmount = $totalAmount + $amount;
            $totalVatAmount = $totalVatAmount + $vatAmount;
            $totalNonVat = $totalNonVat + $nonVat;
            $totalVatableAmount = $totalVatableAmount + $vatableAmount;
        }

        $_SESSION['totalAmount'] = $totalAmount;
        $_SESSION['totalVatAmount'] = $totalVatAmount;
        $_SESSION['totalNonVat'] = $totalNonVat;
        $_SESSION['totalVatableAmount'] = $totalVatableAmount;
    
    }

?>

<html>
<head>
    
    <title> TAS Tradesoft - Expense Report </title>
    <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="stylesheet" href="css/uikit.css" />
            <script src="js/uikit.min.js"></script>
            <script src="js/uikit-icons.min.js"></script>
</head>
<body>
    <?php
        include("navbar.php");
    ?>
    <div class = "uk-container">

        <!--Adding to Records-->

        <div class="uk-grid-small" uk-grid>
            <div class="uk-width-1-6@m">
                <h3 class="uk-heading-bullet">
                <?php
                    if($_GET['sn'] > 0)
                        echo "Edit Expense";
                    else
                        echo "Add Expense";
                ?>
                </h3>
                <div class="uk-card uk-card-default uk-card-body">
                    <form class="uk-form-horizontal uk-margin-small" 
                    action= 
                    <?php 
                        if($_GET['sn'] != 0)
                        {
                            echo "editreceipt.php";
                        }
                        else
                        {
                            echo "admin.php";
                        }
                    ?>
                    method="POST" autocomplete="off">
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-employee">Employee</label>
                            <div class="uk-form-controls">
                                <select class="uk-select uk-form-small" name="employee">
                                    <?php
                                        if($sqllresult = mysqli_query($conn,$sqll))
                                        {
                                            if($_GET['sn'] != 0)
                                                $employeeText = $rowUpdate['employee']; 
                                            else
                                                $employeeText = "";
                                                
                                            echo "<option value='".$employeeText."' selected disabled>".$employeeText."</option>";

                                            
                                            while($rowl = mysqli_fetch_array($sqllresult))
                                            {
                                                echo "<option value='".$rowl['first_name']." ".$rowl['last_name']."'>".$rowl['last_name']. ", ". $rowl['first_name']."</option>";
                                            }
                                        }
                                    ?>
                                </select> 
                            </div>
                        </div>


                        <?php
                            if($_GET['sn'] != 0)
                            {
                                $tintext = "'".$rowUpdate['tin']."'"; 
                                $storeText = "'".$rowUpdate['storename']."'";
                                $typeText = $rowUpdate['type'];
                                $nonVatText = $rowUpdate['nonVat'];  
                                $amountText = $rowUpdate['amount'];
                                $address = "'".$rowUpdate['address']."'"; 
                                $typeText = $rowUpdate['type']; 
                                $vatText = $rowUpdate['vat'] * 100; 
                            }
                            else
                            {
                                $tintext = " ";
                                $storeText = " ";
                                $nonVatText = " "; 
                                $amountText = " "; 
                                $address = " ";
                                $typeText = " ";
                                $vatText = " "; 
                                $typeText = "";
                            }
                        ?>
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-text">Tin</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-horizontal-text" name="tin" type="text"; placeholder = <?php echo $tintext; ?>>
                            </div>
                        </div>
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-date">Date</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-horizontal-date" name="date" type="text" onfocus="(this.type='date')"  placeholder = "<?php echo $TODAY; ?>">
                            </div>
                        </div>
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-text">Store Name</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-horizontal-text" name="storename" type="text"; placeholder = <?php echo $storeText; ?>>
                            </div>
                        </div>

                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-employee">Type</label>
                            <div class="uk-form-controls">
                                <select class="uk-select uk-form-small" name="type">
                                    <?php
                                            echo 
                                                "
                                                    <option value='".$typeText."' selected disabled>"           .$typeText."</option>
                                                    <option value = 'Advances'>                                 Advances </option>
                                                    <option value = 'Advertising Promotion'>                    Advertising Promotion </option>
                                                    <option value = 'Communication'>                            Communication </option>
                                                    <option value = 'Miscellaneous'>                            Miscellaneous </option>
                                                    <option value = 'Pantry'>                                   Pantry </option>
                                                    <option value = 'Permits, Certifications, and Licenses'>    Permits, Certifications, and Licenses </option>
                                                    <option value = 'Repair/Maintenance'>                       Repair/Maintenance </option>
                                                    <option value = 'Representation'>                           Representation </option>
                                                    <option value = 'Stationary/Office Supply'>                 Stationary/Office Supply </option>
                                                    <option value = 'Transport'>                                Transport </option>
                                                    <option value = 'Utilities'>                                Utilities </option>
                                                ";                   
                                    ?>
                                </select> 
                            </div>
                        </div>

                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-text">Address</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-horizontal-text" name="address" type="text"; placeholder = <?php echo $address; ?>>
                            </div>
                        </div>
        
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-text1">Amount</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-horizontal-text1" name="amount" type="text"; placeholder = "<?php echo $amountText; ?>">
                            </div>
                        </div>
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-text1">Non VAT Amount</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" ; placeholder = "<?php echo $nonVatText; ?>" ; id="form-horizontal-text1" name="nonVatAmount" type="text">
                            </div>
                        </div>

                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-text1">VAT</label>
                            <div class="uk-form-controls">
                                <select class="uk-select uk-form-small" name="vat">
                                    <?php
                                            echo 
                                                "
                                                    <option value='".$vatText."' SELECTED DISABLED>"             .$vatText. "% VAT </option>
                                                    <option value = '0.12'>                    12% VAT      </option>
                                                    <option value = '0.00'>                     0% VAT      </option>
                                                ";                   
                                    ?>
                                </select> 
                            </div>
                        </div>
                       
                        <input type = "hidden"; value = "<?php echo $_GET['sn']; ?>"; name="sn" />
                        <?php 
                        
                        

                            if($_GET['sn'] != 0) 
                            {
                                echo "<input type = 'submit'; value = 'EDIT'; name = 'editReceipt'; class = 'uk-button uk-button-danger'>";
                            }

                            else
                            {
                                echo "<input type = 'submit'; value = 'ADD'; name = 'submit'; class = 'uk-button uk-button-primary'>";
                            }
                            
                        ?>
                    </form>
                </div>
                <?php  
                    echo "<u><strong>Total GROSS EXPENSE: ₱". number_format($totalAmount,2)."</strong></u><br>";

                    echo "<u><strong>Total NET AMOUNT: ₱".number_format($totalVatAmount,2)."</strong></u><br>";//Total VAT amount change by NET AMOUNT

                    echo "<u><strong>Total NON VATABLE AMOUNT: ₱".number_format($totalNonVat,2)."</strong></u><br>";

                    echo "<u><strong>Total VATABLE AMOUNT: ₱".number_format($totalVatableAmount,2)."</strong></u><br>";
                
                    echo "<u><strong>REQUIRED AMOUNT: ₱".number_format($requiredamount,2)."</strong></u><br>";

                    echo "<u><strong>REMAINING BALANCE: ₱".number_format($remainbalan,2)."</strong></u><br>";

                    if($remainbalan <= 0)
                    {
                        echo "Quota Reached !";
                    }
                ?>
            </div>
            <div class="uk-width-expand@m">
                <form action="admin.php" method ="post" style = "margin-bottom: 0px;" autocomplete="off">
                    <div class="uk-inline">
                        <div class="uk-margin">
                            <label class="uk-form-label">Employee</label>
                            <select class="uk-select uk-form-small" style="min-width:200px" name ="employee">
                                <?php
                                    if($sqllresult = mysqli_query($conn,$sqll))
                                    {
                                        echo "<option disabled>EMPLOYEE</option>";
                                        echo "<option value=''>ALL</option>";
                                        while($rowl = mysqli_fetch_array($sqllresult))
                                        {
                                            echo "<option value='".$rowl['first_name']." ".$rowl['last_name']."'>".$rowl['last_name']. ", ". $rowl['first_name']."</option>";
                                        }
                                    }
                                ?>
                            </select> 
                        </div>
                    </div>

                    <div class="uk-inline">
                        <div uk-form-custom="target: > * > span:first-child" >
                            <div class="uk-margin">
                                <label class="uk-form-label" for="form-horizontal-date">From Date</label>
                                <div class="uk-form-controls">
                                    <input class="uk-input uk-form-small"  id="from_date" name="from_date" type="date">
                                </div>
                            </div>       
                        </div>
                        <div uk-form-custom="target: > * > span:first-child" >
                            <div class="uk-margin">
                                <label class="uk-form-label" for="form-horizontal-date">To Date</label>
                                <div class="uk-form-controls">
                                    <input class="uk-input uk-form-small" id="to_date" name="to_date" type="date">
                                </div>
                            </div>       
                        </div>
                    </div>

                    <div class="uk-inline uk-form-small">
                        <div uk-form-custom="target: > * > span:first-child">
                            <select     name= "typefilter">
                                <option value="" disabled selected>ALL TYPES</option>
                                <option value="Advances">Advances</option>
                                <option value="Advertising Promotion">Advertising Promotion</option>
                                <option value="Communication">Communication</option>
                                <option value="Miscellaneous">Miscellaneous</option>
                                <option value="Pantry">Pantry</option>
                                <option value="Permits, Certification, and Licenses">Permits, Certification, and Licenses</option>
                                <option value="Repair/Maintenance">Repair/Maintenance</option>
                                <option value="Representation">Representation</option>
                                <option value="Stationary/Office Supply">Stationary/Office Supply</option>
                                <option value="Transport">Transport</option>
                                <option value="Utilities">Utilities</option>
                            </select>
                            <button class="uk-form-small uk-button uk-button-default" type="button" tabindex="-1">
                                <span></span>
                                <span uk-icon="icon: chevron-down"></span>
                            </button>
                        </div>
                    </div>
                    <div class="uk-inline">
                        <input type = "submit" value="SEARCH" class="uk-input uk-button-primary uk-form-small uk-width-1-1" name="searchemployee"><br/>
                    </div>
                </form>
                <form action="excelTestAdmin.php" method ="post"; style="max-width:100%">
                    <input style="border-style: solid; border-color: dodgerblue;" type = "submit" value = "Export to Excel" <?php $_SESSION['download'] = true;?> class="uk-margin-small-bottom uk-margin-small-top uk-input" style= "max-width:700px">
                </form>
                <div class= "uk-overflow-auto">
                    <p class = "uk-textarea uk-padding-remove uk-divider"; style="min-height: 545px; max-height: 545px;"; >
                        <?php
                            include ("data.php");     
                        ?>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-group-edit-receipt" uk-modal>
        <div class="uk-modal-dialog">
            <div class="uk-modal-header">
                <h2 class="uk-modal-title">Edit Receipt</h2>
            </div>
            <div class="uk-modal-body">
                <form method= "post" action="editreceipt.php">
                    <div class="uk-card-body">
                        <div class="form-input">
                            <label class="uk-form-label" for="form-horizontal-text">Tin</label><br style="max-height:2px">
                            <input class="uk-input uk-form-width-medium" type="text" name="tin" placeholder=<?php echo $row['tin'] ?>> 
                        </div>
                        <div class="form-input">
                            <label class="uk-form-label" for="form-horizontal-text">Store Name</label><br style="max-height: 2px">   
                            <input class="uk-input uk-form-width-medium" type="text" name="storename" placeholder= <?php echo $row['storename'] ?>> 
                        </div>
                        <div class="form-input uk-child-width-9-10">
                            <label class="uk-form-label" for="form-horizontal-text">Address</label><br style="max-height:2px">
                            <input class="uk-input uk-form-width-medium" type="text" name="address" placeholder=<?php echo $row['address'] ?>> 
                        </div>
                        <div class="form-input">
                            <label class="uk-form-label" for="form-horizontal-text">Type</label><br style="max-height:5px">
                            <select name = "type" class="uk-select" class="uk-margin-small-bottom uk-margin-small-top uk-select uk-form-small"; style="max-width:50%">
                                <option value= "" disabled selected><?php echo $row['type'] ?></option>
                                <option value= "Advances">Advances</option>
                                <option value= "Advertising Promotion">Advertising Promotion</option>
                                <option value= "Communication">Communication</option>
                                <option value= "Miscellaneous">Miscellaneous</option>
                                <option value= "Pantry">Pantry</option>
                                <option value= "Permits, Certifications, and Licenses">Permits, Certifications, and Licenses</option>
                                <option value= "Repair/Maintenance">Repair/Maintenance</option>
                                <option value= "Representations">Representation</option>
                                <option value= "Stationary/Office Supplies">Stationary/Office Supplies</option>
                                <option value= "Transport">Transport</option>
                                <option value= "Utilities">Utilities</option>
                            </select>
                        </div>
                        <div class="form-input">
                            <label class="uk-form-label" for="form-horizontal-text">Date of Transaction</label><br style="max-height:2px">
                            <input class="uk-input uk-form-width-medium" type="date" name="date" placeholder=<?php echo $row['birthdate'] ?>; style="max-width:40%">
                        </div>
                        <div class="form-input uk-child-width-1-4">
                            <label class="uk-form-label" for="form-horizontal-text">Amount</label><br style="max-height:2px">
                            <input class="uk-input uk-form-width-medium" type="text" name="amount" placeholder=<?php echo $row['amount'] ?> > 
                        </div>
                        <div class="form-input uk-child-width-1-4">
                            <label class="uk-form-label" for="form-horizontal-text">Non-Vatable</label><br style="max-height:2px">
                            <input class="uk-input uk-form-width-medium" type="nonVat" name="nonVat" placeholder=<?php echo $row['department'] ?>> 
                        </div>
                        <div class="form-input uk-child-width-1-4">
                            <label class="uk-form-label" for="form-horizontal-text">VAT</label><br style="max-height:2px">
                            <select name = "vat" class="uk-select" class="uk-margin-small-bottom uk-margin-small-top uk-select uk-form-small">
                                <option value= "" disabled selected> <?php echo $row['vat'] * 100 . "%" ?></option>
                                <option value="0.00">0%</option>
                                <option value="0.12">12%</option>
                            </select>
                        </div>
                        <br style="max-height:2px">
                <div class="uk-card-footer">
                    <button type= "submit" name="editReceipt" class="uk-button uk-button-primary ">Edit</button>
                </div>
                </div>
            </form>
    </div> 

</body>

</html>

