<?php
    session_start();
    error_reporting(E_PARSE | E_ERROR);

    if (!isset($_SESSION['logged_in'])) 
    {
        header("location: login.php");
    }

    include ('connect.php');

    $userEmployee=$_SESSION['user'];
    $position=$_SESSION['position'];
    $userID=$_SESSION['idnumber'];

    $sql2 = "SELECT * FROM accounts WHERE user = '$userEmployee'";
    $sql2result = mysqli_query($conn,$sql2);
    $row = mysqli_fetch_array($sql2result);
    $requiredamount = $row['requiredamount'];

    $sqlUpdate = "SELECT * FROM receipt WHERE sn = ". $_GET['sn'];

    $sqlUpdateResult = mysqli_query($conn,$sqlUpdate);
    $rowUpdate = mysqli_fetch_array($sqlUpdateResult);

    if($_SESSION['position']=="Admin")
    {
        header("location: admin.php");
    }

    

    $TODAY=date('m/d/Y');

    if(isset($_POST['submit']))
    {
        $DATE = $_POST['date'];
        $STORENAME = $_POST['storename'];
        $TIN = $_POST['tin'];
        $TYPE = $_POST['type'];
        $ADDRESS = $_POST['storeaddress'];
        $AMOUNT = $_POST['amount'];
        $VAT = $_POST['vat'];
        $NonVatAmount = $_POST['NonVatAmount'];
        $VATAMOUNT = ($AMOUNT - $NonVatAmount)/(1+$VAT); //VATAMOUNT CHANGE TO NET AMOUNT
        $vatableAmount = ($VATAMOUNT*$VAT);//new VATTABLE amount

        #employee or admin input vat in decimal

        #default vat is 12 percent

        if (empty($DATE))
        {
            $sql = "INSERT INTO receipt (date, storename, tin, address, type, amount, vat, nonVat, vatAmount, employee,vatableAmount)
                    VALUES (NOW(), '$STORENAME', '$TIN', '$ADDRESS', '$TYPE', '$AMOUNT', '$VAT', '$NonVatAmount', '$VATAMOUNT', '$userEmployee','$vatableAmount')";
        }
        else{
            $sql = "INSERT INTO receipt (date, storename, tin, address, type, amount, vat, nonVat, vatAmount, employee,vatableAmount)
                    VALUES ('$DATE', '$STORENAME', '$TIN', '$ADDRESS', '$TYPE', '$AMOUNT', '$VAT', '$NonVatAmount', '$VATAMOUNT', '$userEmployee','$vatableAmount')";
        }

        //$sql3 = "UPDATE accounts SET requiredamount = GREATEST(0,requiredamount - '$AMOUNT') WHERE user = '$userEmployee' AND requiredamount > 0";

        if( empty($STORENAME) || empty($AMOUNT) || empty($VAT) )
            {
                echo "<div class='uk-alert-danger' uk-alert><a class='uk-alert-close' uk-close></a>You did not fill out the required fields.</div>";
            }

        else if ($conn->query($sql) === TRUE)
            {
                echo "<div class='uk-alert-success' uk-alert><a class='uk-alert-close' uk-close></a>Expense recorded. Click to Close</div>";
            }

        else
            {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
        
        header("Location: sample.php");
        exit;
    }

        $from_date = $_POST['from_date'];
        $to_date = $_POST['to_date'];
        $typefilter = $_POST['typefilter'];

        $q  = "SELECT sum(amount) AS total FROM receipt";
        $q2 = "SELECT sum(vatAmount) as total1 FROM receipt";
        $q3 = "SELECT sum(vatableAmount) as total2 FROM receipt";
        $q4 = "SELECT sum(nonVat) as total3 FROM receipt";

        $conditions = array();

        if(!empty($from_date) AND !empty($to_date))
        {
            $conditions[] = "(date BETWEEN '$from_date' AND '$to_date')";
        }
        else
        {
            $conditions[] = "MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE())";
        }

        if(!empty($typefilter))
        {
            $conditions[] = "type='$typefilter'";
        }


            $conditions[] = "employee = '$userEmployee'";

            $sqlgross = $q;
            $sqlvat = $q2;
            $sqlvatable = $q3;
            $sqlnonvat = $q4;

            if (count($conditions)>0)
            {
                $sqlgross .= " WHERE " . implode(' AND ',$conditions);
                $sqlvat .= " WHERE " . implode(' AND ',$conditions);
                $sqlvatable .= " WHERE " . implode(' AND ',$conditions);
                $sqlnonvat .= " WHERE " . implode(' AND ',$conditions);
            }

    $_SESSION['sqlgross'] = $sqlgross;
    $_SESSION['sqlvat'] = $sqlvat;
    $_SESSION['sqlvatable'] = $sqlvatable;
    $_SESSION['sqlnonvat'] = $sqlnonvat;
    

    $sql2 = "SELECT * FROM accounts WHERE user = '$userEmployee'";
    $sql2result = mysqli_query($conn,$sql2);
    $row = mysqli_fetch_array($sql2result);
    $requiredamount = $row['requiredamount'];

    $result1 = mysqli_query($conn,$sqlgross);
    $row1 = mysqli_fetch_array($result1);
    $amount1 = $row1['total'];

    $result2=mysqli_query($conn,$sqlvat);
    $row2 = mysqli_fetch_array($result2);
    $amount2 = $row2['total1'];

    $result3=mysqli_query($conn,$sqlvatable);
    $row3 = mysqli_fetch_array($result3);
    $amount3 = $row3['total2'];

    $result4=mysqli_query($conn,$sqlnonvat);
    $row4 = mysqli_fetch_array($result4);
    $amount4 = $row4['total3'];

    $remainbalan = ($requiredamount - $amount1);

    $conn->close();
?>

<html>
<head>

    <title> TAS Tradesoft - Expense Report </title>
    <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/uikit.css" />
        <script src="js/uikit.min.js"></script>
        <script src="js/uikit-icons.min.js"></script>
        <script type="text/javascript" src="js/timeScripts.js"></script>
</head>
<body onLoad = "renderTime();">
    <div id = "clockDisplay"></div>
        <?php
            include("navbar.php");
        ?>
    <div class="uk-container">
        <div class="uk-grid-small" uk-grid>
            <!-- 1st Column -->
            <div class="uk-child-width-3-10@m">
            <?php

                if($_GET['sn'] != 0)
                {
                    $mesHeader = "Edit Expense";
                    $uDate = $rowUpdate['date'];
                    $uTin = $rowUpdate['tin'];
                    $uStoreName = $rowUpdate['storename'];
                    $uAddress = $rowUpdate['address'];
                    $uType = $rowUpdate['type'];
                    $uAmount = $rowUpdate['amount'];
                    $uNonVAT = $rowUpdate['nonVat'];
                    $uVAT = $rowUpdate['vat'];
                    $uVatAmount = $rowUpdate['vatAmount'];
                    $otherLink = "editReceipt.php";

                }
                else
                {
                    $mesHeader =    "Add Expense";
                    $uDate =        "$TODAY";
                    $uTin =         "";
                    $uStoreName =   "";
                    $uAddress =     "";
                    $uType =        "";
                    $uAmount =      "";
                    $uNonVAT =      "";
                    $uVAT =         "";
                    $uVatAmount =   "";
                    $otherLink =    "sample.php";
                    
                }
            ?>
            
                <h3 class="uk-heading-divider uk-text-center uk-text-bold uk-text-emphasis"><?php echo $mesHeader; ?></h3>
                <div class="uk-card uk-card-default uk-card-body">
                    <form class="uk-form-horizontal uk-margin-small" action=<?php echo $otherLink; ?> method="POST" autocomplete="off">

                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-date">Date</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-horizontal-date" name="date" type="text" onfocus="(this.type='date')"  placeholder = "<?php echo $uDate; ?>">
                            </div>
                        </div>

                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-text">Tin</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-horizontal-text" name="tin" type="text"; placeholder = <?php echo $uTin; ?>>
                            </div>
                        </div>

                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-text">Store Name</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-horizontal-text" name="storename" type="text"; placeholder = <?php echo $uStoreName; ?>>
                            </div>
                        </div>

                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-text">Address</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-horizontal-text" name="storeaddress" type="text"; placeholder = <?php echo $uAddress; ?>>
                            </div>
                        </div>

                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-employee">Type</label>
                            <div class="uk-form-controls">
                                <select class="uk-select uk-form-small" name="type">
                                    <?php
                                            echo 
                                                "
                                                    <option value='".$uType."' selected disabled>"              .$uType."                               </option>
                                                    <option value = 'Advances'>                                 Advances                                </option>
                                                    <option value = 'Advertising Promotion'>                    Advertising Promotion                   </option>
                                                    <option value = 'Communication'>                            Communication                           </option>
                                                    <option value = 'Miscellaneous'>                            Miscellaneous                           </option>
                                                    <option value = 'Pantry'>                                   Pantry                                  </option>
                                                    <option value = 'Permits, Certifications, and Licenses'>    Permits, Certifications, and Licenses   </option>
                                                    <option value = 'Repair/Maintenance'>                       Repair/Maintenance                      </option>
                                                    <option value = 'Representation'>                           Representation                          </option>
                                                    <option value = 'Stationary/Office Supply'>                 Stationary/Office Supply                </option>
                                                    <option value = 'Transport'>                                Transport                               </option>
                                                    <option value = 'Utilities'>                                Utilities                               </option>
                                                ";                   
                                    ?>
                                </select> 
                            </div>
                        </div>

                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-text1">Amount</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-horizontal-text1" name="amount" type="text"; placeholder = <?php echo $uAmount; ?>>
                            </div>
                        </div>

                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-text1">Non VAT Amount</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-horizontal-text1" name="NonVatAmount" type="text"; placeholder = <?php echo $uNonVAT; ?>>
                            </div>
                        </div>

                            <div class="uk-margin">
                                <label class="uk-form-label" for="form-horizontal-text1">VAT</label>
                                <div class="uk-form-controls">
                                    <input class="uk-input" id="form-horizontal-text1" list="vat" value="0.12" name="vat" type="text"; placeholder = <?php echo $uVAT; ?>>
                                    <datalist id="vat">
                                        <option value="0.12" name="vat" >12% VAT
                                        <option value="0.05" name="vat" >5% VAT
                                        <option value="0.00" name="vat" >NON VAT
                                    </datalist>
                                </div>
                            </div>

                                <!-- <div class="uk-margin">
                                    <label class="uk-form-label" for="form-horizontal-text1">VAT Amount</label>
                                    <div class="uk-form-controls">
                                        <input class="uk-input" id="form-horizontal-text1" name="vatAmount" type="text"; placeholder = <?php echo $uVatAmount; ?>>
                                    </div>
                                </div> -->
                            
                            <?php
                            
                            if($_GET['sn'] != 0)
                            {
                                $mesButton = "edit";
                                $buttonStyle = "uk-button uk-button-danger";
                            }
                            else
                            {
                                $mesButton = "add";
                                $buttonStyle = "uk-button uk-button-primary";
                            }
                            
                            ?>
                                                   
                            <input type = "hidden"; value = "<?php echo $_GET['sn']; ?>"; name="sn" />
                            <input type = "submit" value=<?php echo $mesButton;?> class="<?php echo $buttonStyle; ?>"; name="submit" onclick = "return mess()">
                        </form>
                    </div>
                    <?php
                        echo "<u><strong>Total GROSS EXPENSE: ₱". number_format($amount1,2)."</strong></u><br>";
                        echo "<u><strong>Total NET AMOUNT: ₱".number_format($amount2,2)."</strong></u><br>";//Total VAT amount change by NET AMOUNT
                        echo "<u><strong>Total NON VATABLE AMOUNT: ₱".number_format($amount4,2)."</strong></u><br>";
                        echo "<u><strong>Total VATABLE AMOUNT: ₱".number_format($amount3,2)."</strong></u><br>";
                        echo "<u><strong>REQUIRED AMOUNT: ₱".number_format($requiredamount,2)."</strong></u><br>";
                        echo "<u><strong>REMAINING BALANCE: ₱".number_format($remainbalan,2)."</strong></u><br>";

                        if($remainbalan <= 0)
                        {
                            echo "Quota Reached !";

                        }

                    ?>

                </div>
                <!-- End of 1st Column -->

                <!-- 2nd Window -->

                <div class="uk-width-expand@m">
                    <form action="sample.php" method ="post" style = "margin-bottom: 0px;">
                        <!--<div class= "uk-card-title" style="font-size: 20px;">Filters</div> -->
                        <div class="uk-inline-small">
                            <div uk-form-custom="target: > * > span:first-child" >
                                <div class="uk-margin">
                                    <label class="uk-form-label uk-text-bold" for="form-horizontal-date">From Date</label>
                                    <div class="uk-form-controls">
                                        <input class="uk-input uk-form-small"  id="from_date" name="from_date" type="date">
                                    </div>
                                </div>
                            </div>
                            <div uk-form-custom="target: > * > span:first-child" >
                                <div class="uk-margin">
                                    <label class="uk-form-label uk-text-bold" for="form-horizontal-date">To Date</label>
                                    <div class="uk-form-controls">
                                        <input class="uk-input uk-form-small" id="to_date" name="to_date" type="date">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-inline">
                            <div uk-form-custom="target: > * > span:first-child">
                                <select name= "typefilter">
                                    <option value="">TYPE</option>
                                    <option value="Advances">Advances</option>
                                    <option value="Advertising Promotion">Advertising Promotion</option>
                                    <option value="Communication">Communication</option>
                                    <option value="Miscellaneous">Miscellaneous</option>
                                    <option value="Pantry">Pantry</option>
                                    <option value="Permits, Certification, and Licenses">Permits, Certification, and Licenses</option>
                                    <option value="Repair/Maintenance">Repair/Maintenance</option>
                                    <option value="Representation">Representation</option>
                                    <option value="Stationary/Office Supply">Stationary/Office Supply</option>
                                    <option value="Transport">Transport</option>
                                    <option value="Utilities">Utilities</option>
                                </select>
                                <button class="uk-button uk-button-default uk-form-small" style = "width=200px" type="button" tabindex="-1">
                                    <span></span>
                                    <span uk-icon="icon: triangle-down"></span>
                                </button>
                            </div>
                        </div>

                        <div class="uk-inline">
                            <input type = "submit" value="Search" class="uk-input uk-button-primary uk-form-small" name="search"><br/>
                        </div>

                            <!--<div class="uk-inline">
                                <form action="excel.php" method ="post">
                                        <input style="border-style: solid; border-color: dodgerblue; "type = "submit" value = "Export to Excel" <?php $_SESSION['download'] = true;?> class="uk-margin-small-bottom uk-margin-small-top uk-input">
                                </form>
                            </div>-->
                    </form>
                    <form action="excelTest.php" method ="post"; style="max-width: 100%">
                        <input style="border-style: solid; border-color: dodgerblue; " name = "export_excel" type = "submit" value = "Export to Excel" <?php $_SESSION['download'] = true;?> class="uk-margin-small-bottom uk-margin-small-top uk-input">
                        <input type= "hidden" name= "SQL" value = $sql>
                    </form>

                    <p class = "uk-textarea uk-padding-remove uk-width-auto@m"; style="max-height: 585px; max-width: 100%;" >
                        <?php
                            include "data.php";
                        ?>
                    </p>
                </div>
                <!-- End of 2nd Column -->
        </div>
    </div>

    <div id="settingsMod" uk-modal>
        <div class="uk-modal-dialog">
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <div class="uk-modal-header">
                <h2 class="uk-modal-title">Modal Title</h2>
            </div>
            <div class="uk-modal-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
                <button class="uk-button uk-button-primary" type="button">Save</button>
            </div>
        </div>
    </div>
</body>
</html>
