Edit Records
        <!--Adding to Records-->
        <!-- <div class="uk-grid-small" uk-grid>
            <div class="uk-width-1-6@m"; style="float:left">
                <div class="uk-card uk-card-default uk-card-body">
                    <h3 class="uk-heading-bullet">

                        <?php
                            if($_GET['id'] != 0)
                                echo "Edit User";
                            else
                                echo "Add User";
                        ?>

                    </h3>
                    <form class="uk-form-horizontal uk-margin-small" 
                    action= 
                    <?php 
                        if($_GET['id'] != 0)
                        {
                            echo "editprofile.php";
                        }
                        else
                        {
                            echo "userlistpage.php";
                        }
                    ?>
                    method="POST" autocomplete="off"><?php
                            if($_GET['id'] != 0)
                            {
                                $idnumber = "'".$row['idnumber']."'"; 
                                $fname = "'".$row['first_name']."'"; 
                                $lname = $row['last_name']; 
                                $bdate = $row['birthdate']; 
                                $haddress = $row['home_Address']; 
                                $phic = $row['phic']; 
                                $pagibig = $row['pagibig'];
                                $sss = $row['sss'];
                                $tin = $row['tin'];
                                $dept = $row['department'];
                                $postype = $row['position_type'];
                                $hdate = $row['datehired'];
                                $quota = $row['requiredamount'];
                                $user = $row['user'];
                                $pass = $row['password'];
                            }

                            else
                            {
                                $idnumber = " ";
                                $fname = " "; 
                                $lname = " ";
                                $bdate = " "; 
                                $haddres = " ";
                                $phic = " ";
                                $pagibig = " ";
                                $sss = " ";
                                $tin = " ";
                                $dept = " ";
                                $postype = " ";
                                $hdate = " ";
                                $quota = " ";
                                $user = " ";
                                $pass = " ";
                            }                
                        ?>
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-text">ID Number</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-horizontal-text" name="idnumber" type="text"; placeholder = <?php echo $idnumber; ?>>
                            </div>
                        </div>

                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-text">Last Name</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-horizontal-text" name="last_name" type="text"; placeholder = <?php echo $lname; ?>>
                            </div>
                        </div>

                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-text">First Name</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-horizontal-text" name="first_name" type="text"; placeholder = <?php echo $fname; ?>>
                            </div>
                        </div>

                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-date">Birth Date</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-horizontal-date" name="date" type="date" placeholder = "<?php echo $bdate; ?>">
                            </div>
                        </div>

                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-text1">Home Address</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" ; placeholder = "<?php echo $haddress; ?>" ; id="form-horizontal-text1" name="home_address" type="text">
                            </div>
                        </div>

                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-employee">Position Type</label>
                            <div class="uk-form-controls">
                                <select class="uk-select uk-form-small" name="postype">
                                    <?php

                                            echo 
                                                "
                                                    <option value= $postype selected disabled>               $postype           </option>
                                                    <option value = 'Admin'>                                 Admin              </option>
                                                    <option value = 'Employee'>                              Employee           </option>                           
                                            
                                                ";                   

                                    ?>
                                </select> 
                            </div>
                        </div>

                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-date">Department</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-horizontal-date" name="department" type="text" placeholder = "<?php echo $dept; ?>">
                            </div>
                        </div>

                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-date">Hire Date</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-horizontal-date" name="hdate" type="date" placeholder = "<?php echo $bdate; ?>">
                            </div>
                        </div>
                            
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-text1">PhilHealth</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" ; placeholder = "<?php echo $phic; ?>" ; id="form-horizontal-text1" name="phic" type="text">
                            </div>
                        </div>

                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-text1">Pag-ibig</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" ; placeholder = "<?php echo $pagibig; ?>" ; id="form-horizontal-text1" name="pagibig" type="text">
                            </div>
                        </div>

                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-text1">SSS</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" ; placeholder = "<?php echo $sss; ?>" ; id="form-horizontal-text1" name="sss" type="text">
                            </div>
                        </div>

                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-text1">Tin</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" ; placeholder = "<?php echo $tin; ?>" ; id="form-horizontal-text1" name="tin" type="text">
                            </div>
                        </div>
                       
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-text1">Quota</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-horizontal-text1" name="quota" type="text"; placeholder = "<?php echo $quota; ?>">
                            </div>
                        </div>

                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-horizontal-text1">Password</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="form-horizontal-text1" name="pass" type="password"; placeholder = "<?php echo $pass; ?>">
                            </div>
                        </div>

                        <input type = "hidden"; value = "<?php echo $_GET['id']; ?>"; name="id" />
                        <?php 
                                                
                            if($_GET['id'] != 0) 
                            {
                                echo "<input type = 'submit'; value = 'EDIT'; name = 'editprofile'; class = 'uk-button uk-button-danger'>";
                            }
                            
                            else
                                {
                                    echo "<input type = 'submit'; value = 'ADD'; name = 'submit'; class = 'uk-button uk-button-primary'>";
                                }
                            
                        ?>
                    </form>
                </div>
            </div> -->