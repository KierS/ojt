<?php
    session_start();
  include('connect.php');

  $output = '';
  if(isset($_POST["export_excel"]))
  {
      $sql = $_SESSION['SQL'];
      $result = mysqli_query($conn, $sql);

      if(mysqli_num_rows($result) > 0)
      {
          $output .= '
            <table class = "table" bordered="1">
                <tr>
                    <th>SN</th>
                    <th>Date</th>
                    <th>Employee</th>
                    <th>Store Name</th>
                    <th>Type</th>
                    <th>Gross Expense</th>
                    <th>VAT Rate</th>
                    <th>NON Vatables</th>
                    <th>Net Amount</th>
                    <th>VATable Amount</th>
                </tr>
          ';
          while($row = mysqli_fetch_array($result))
          {
              $output .= '
                <tr>
                    <td></td> 
                    <td>'.$row["date"].'</td>
                    <td>'.$row["employee"].'</td>
                    <td>'.$row["storename"].'</td>
                    <td>'.$row["type"].'</td>
                    <td>'.$row["amount"].'</td>
                    <td>'.$row["vat"].'</td>
                    <td>'.$row["vatAmount"].'</td>
                    <td>'.$row["nonVat"].'</td>
                    <td>'.$row["vatableAmount"].'</td>
                </tr>
              ';
          }

          $output .= '</table>';
          header("Content-Type: application/xls");
          header("Content-Disposition: attachment; filename=ExcelTest.xls");
          echo $output;
      }
  }
?>
