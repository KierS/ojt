<?php

    $idNum = "";
    $fName = "";
    $lName = "";
    $bDate = "";
    $address = "";
    $pagibig = "";
    $phic = "";
    $sss = "";
    $tin = "";
    $position = "";
    $dept = "";
    $dateHired = "";
    $quota = "";
    $password = "";

    $errors = array();

	$servername = "localhost";
	$username = "root";
	$password1 = "";
	$dbname = "tas_receipt";

    $conn = new mysqli($servername, $username, $password, $dbname);

    if(isset($_POST['register']))
    {
        $idNum = trim($_POST['idnumber']);
        $fName = $_POST['fName'];
        $lName = $_POST['lName']; 
        $bDate = $_POST['birthdate'];
        $address = $_POST['homeaddress'];
        $pagibig = $_POST['pagibig'];
        $phic = $_POST['philhealth'];
        $sss = $_POST['sss'];
        $tin = $_POST['tin'];
        $position = $_POST['type'];
        $dept = $_POST['department'];
        $dateHired = $_POST['datehired'];
        $quota = $_POST['requiredamount'];
        $password1 = $_POST['password_1'];

        
        if (empty($idNum))
        {
            array_push($errors, "First Name is required");
        }
        if (empty($fName))
        {
            array_push($errors, "First Name is required");
        }
        if (empty($lName))
        {
            array_push($errors, "Last Name is required");
        }
        if (empty($bDate))
        {
            array_push($errors, "Birth Date is required");
        }
        if (empty($address))
        {
            array_push($errors, "Address is required");
        }
        if (empty($pagibig))
        {
            array_push($errors, "Pagibig is required");
        }
        if (empty($phic))
        {
            array_push($errors, "PHIC is required");
        }
        if (empty($sss))
        {
            array_push($errors, "SSS is required");
        }
        if (empty($tin))
        {
            array_push($errors, "Tin is required");
        }
        if (empty($position))
        {
            array_push($errors, "Position is required");
        }
        if (empty($dept))
        {
            array_push($errors, "Department is required");
        }
        if (empty($dateHired))
        {
            array_push($errors, "Date Hired is required");
        }
        if (empty($quota))
        {
            array_push($errors, "Quota is required");
        }
        if (empty($password1))
        {
            array_push($errors, "Password is required");
        }

        if (count($errors) == 0)
        {
            $password1 = md5($password1);
            $fullName = $fName." ".$lName;

            $sql="INSERT INTO accounts (idnumber, first_name, Last_name, birthdate, home_Address, phic, pagibig, sss, tin, department, position_type, datehired, requiredamount, user, password)
                    VALUES('$idNum','$fName','$lName','$bDate','$address','$phic','$pagibig','$sss','$tin','$dept','$position','$dateHired', '$quota', '$fullName','$password1')";

            mysqli_query($conn, $sql);

            //echo $idNum."<br>".$fName."<br>".$lName."<br>".$address."<br>".$bDate."<br>".$pagibig."<br>".$phic."<br>".$sss."<br>".$tin."<br>".$position."<br>".$dept."<br>".$dateHired."<br>".$password;
            // echo $_POST['idnumber']." ";
            // echo $_POST['fName']." ";
            // echo $_POST['lName']." ";
            // echo $_POST['birthdate']." ";
            // echo $_POST['homeaddress']." ";
            // echo $_POST['pagibig']." ";
            // echo $_POST['philhealth']." ";
            // echo $_POST['sss']." ";
            // echo $_POST['tin']." ";
            // echo $_POST['department']." ";
            // echo $_POST['type']." ";
            // echo $_POST['datehired']." ";
            // echo $_POST['requiredamount']." ";
            // echo $_POST['password_1']." ";

            header("Location: userlistpage.php");
            exit;
        }
    }
    
?>