<?php

session_start();




 if (!isset($_SESSION['logged_in'])) 
  {
    header("location: logout.php");    
  }


else
{
 
 include ('connect.php');
 ob_start();

  $position=$_SESSION['position'];


  if ($conn->connect_error) 
  {
      die("Connection failed: " . $conn->connect_error);
  } 

  if(isset($_POST['search']))
  {
    if ($position=="admin")
    {
    $month = $_POST['month'];
    $typefilter = $_POST['typefilter'];

    $namefilter = $_POST['nameToSearch'];
    $_SESSION['nameToSearch']=$namefilter;
    $sql="SELECT sn,date,storename,type,amount,vat, vatAmount, nonVat, employee,vatableAmount FROM receipt WHERE month(date) ='$month'|| type='$typefilter' || 
    employee= '$namefilter' ";
    $sql1="SELECT sum(amount) AS total FROM receipt WHERE month(date) ='$month'|| type='$typefilter' || employee = '$namefilter' ";
    $sql2="SELECT sum(vatAmount) AS total1 FROM receipt WHERE month(date) ='$month'|| type='$typefilter' || employee = '$namefilter' ";
    $sql3="SELECT sum(expense) AS total2 FROM receipt WHERE month(date) ='$month'|| type='$typefilter' || employee = '$namefilter' ";
    $sql4="SELECT requiredamount  AS Rbalance FROM accounts WHERE user='$namefilter' ";
    

    echo "<td style='width: 100px;' colspan=6>Employee: ".$namefilter."<br</td>";
    }
    else
    {
    $month = $_POST['month'];
    $typefilter = $_POST['typefilter'];
    $namefilter = $_SESSION['user'];
    $sql="SELECT sn,date,storename,type,amount,vat, vatAmount, nonVat, employee,vatableAmount FROM receipt WHERE (month(date) ='$month'|| type='$typefilter') AND 
    employee= '$namefilter' ";
    $sql1="SELECT sum(amount) AS total FROM receipt WHERE (month(date) ='$month'|| type='$typefilter') AND employee = '$namefilter' ";
    $sql2="SELECT sum(vatAmount) AS total1 FROM receipt WHERE (month(date) ='$month'|| type='$typefilter') AND employee = '$namefilter' ";
    $sql3="SELECT sum(expense) AS total2 FROM receipt WHERE (month(date) ='$month'|| type='$typefilter') AND employee = '$namefilter' ";
    $sql4="SELECT requiredamount  AS Rbalance FROM accounts WHERE user='$namefilter' ";

    }
  
  }
  else{

    if ($position=="admin"){
      $sql="SELECT sn,date,storename,type,amount,vat, vatAmount, nonVat,vatableAmount FROM receipt ORDER BY sn DESC ";
      $sql1="SELECT sum(amount) AS total FROM receipt  ";
      $sql2="SELECT sum(vatAmount) AS total1 FROM receipt  ";
      $sql3="SELECT sum(expense) AS total2 FROM receipt  ";
      $sql4="SELECT requiredamount  AS Rbalance FROM accounts WHERE user='' ";
    }
    else
    {
    
    $namefilter = $_SESSION['user'];
    $sql="SELECT sn,date,storename,type,amount,vat, vatAmount, nonVat, employee,vatableAmount FROM receipt WHERE  
    employee= '$namefilter' ";
    $sql1="SELECT sum(amount) AS total FROM receipt WHERE employee = '$namefilter' ";
    $sql2="SELECT sum(vatAmount) AS total1 FROM receipt WHERE employee = '$namefilter' ";
    $sql3="SELECT sum(expense) AS total2 FROM receipt WHERE employee = '$namefilter' ";
    $sql4="SELECT requiredamount  AS Rbalance FROM accounts WHERE user='$namefilter' ";

    }

    }

  if ($result=mysqli_query($conn,$sql))
  {
    
    echo "<table class='uk-table uk-table-hover uk-table-striped uk-table-small'>";
    echo "<tr>
    <th style= 'color:DodgerBlue'>SN</th>
    <th style= 'color:DodgerBlue'>DATE</th>
    <th style= 'color:DodgerBlue'>STORE NAME</th>
    <th style= 'color:DodgerBlue'>TYPE</th>
    <th style= 'color:DodgerBlue'>GROSS EXPENSE</th>
    <th style= 'color:DodgerBlue'>VAT</th>
    <th style= 'color:DodgerBlue'>NET</th>
    <th style= 'color:DodgerBlue'>NON VATABLES</th>
    <th style= 'color:DodgerBlue'>VATABLES</th>
    <th></th>
    </tr>";

    $counter=1;

    while($row = mysqli_fetch_array($result)) 
    {
        $sn = $row['sn'];
        $source = $row['date'];
        $date =new dateTime($source);
        $storename = $row['storename'];
        $type = $row['type'];
        $vatRate =$row['vat'];
        $amount = $row['amount'];
        $vatAmount = $row['vatAmount'];//change by NET AMOUNT
        $nonVat = $row['nonVat'];
        $vatableAmount = $row['vatableAmount'];//new vatable amount

        echo "<tr>
            <td style='width: 100px; text-align:left;'>".$counter."</td>
            <td style='width: 100px;text-align:left;'>".$date->format('M-d-Y')."</td>
            <td style='width: 100px;text-align:left;'>".$storename."</td>
            <td style='width: 150px;text-align:left;'>".$type."</td>
            <td style='width: 100px;'>  ".number_format($amount,2)."</td>
            <td style='width: 70px;'> ".($vatRate)."</td>
            <td style='width: 100px;'>  ".number_format($vatAmount,2)."</td>
            <td style='width: 100px;'>  ".number_format($nonVat,2)."</td>
            <td style='width: 100px;'>  ".number_format($vatableAmount,2)."</td>
         

            </tr>
            </tr>";

           $counter++;

    } 
    
 
 
    

      if ($result1=mysqli_query($conn,$sql1))
      {
        while($row1 = mysqli_fetch_array($result1))
        {
        $amount1 = $row1['total'];

          echo "<tr><td style='width: 100px;' colspan=4>Total Gross Expense:<br></td>";
          echo "<td style='width: 100px;' colspan=1>".number_format($amount1,2)."<br></td></tr>";

  

      
        }
      }


    if ($result2=mysqli_query($conn,$sql2))
      {
        while($row2 = mysqli_fetch_array($result2))
        {
        $amount2 = $row2['total1'];
          echo "<tr><td style='width: 100px;' colspan=4>Total NET:<br></td>";//change from total vat to NET
          echo "<td style='width: 100px;' colspan=1>".number_format($amount2,2)."<br></td></tr>";

      
        }
      }


      if ($result3=mysqli_query($conn,$sql3))
      {
        while($row3 = mysqli_fetch_array($result3))
        {
        $amount3 = $row3['total2'];
          echo "<tr><td style='width: 100px;' colspan=4>Total Net Expense:<br></td>";
          echo "<td style='width: 100px;' colspan=1>".number_format($amount3,2)."<br></td></tr>";
      
        }
      }
      echo "</table>";




        mysqli_free_result($result);
  }
        



        header('Content-Type: application/xls');
        header('Content-Disposition: attachment; filename=Receipt.xls');
        mysqli_close($conn);
        ob_end_flush();
}



?>

